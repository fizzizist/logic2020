# pylint: disable=unused-argument
from django.db.models.signals import post_save
from django.dispatch import receiver
from loguru import logger
from logix.models import RuleUser


@receiver(post_save, sender=RuleUser)
def cascade_rule_unlocks(sender, instance, **kwargs):
    """Function to cascade rule unlocks in cases where two rules being unlocked unlocks a third."""
    logger.info("Cascading rule unlocks.")

    if instance.unlocked and instance.rule.label == "MC1":
        try:
            mc2_rule_user = sender.objects.get(user=instance.user, rule__label="MC2")
            mc_rule_user = sender.objects.get(user=instance.user, rule__label="MC")
            if mc2_rule_user.unlocked and not mc_rule_user.unlocked:
                mc_rule_user.unlocked = True
                mc_rule_user.save()
                logger.info("MC has been unlocked.")
        except sender.DoesNotExist:
            logger.error(
                f"Corresponding MC2 and MC rules do not exist in the database for"
                f"{instance.user}"
            )

    elif instance.unlocked and instance.rule.label == "MC2":
        try:
            mc1_rule_user = sender.objects.get(user=instance.user, rule__label="MC1")
            mc_rule_user = sender.objects.get(user=instance.user, rule__label="MC")
            if mc1_rule_user.unlocked and not mc_rule_user.unlocked:
                mc_rule_user.unlocked = True
                mc_rule_user.save()
                logger.info("MC has been unlocked.")
        except sender.DoesNotExist:
            logger.error(
                f"Corresponding MC1 and MC rules do not exist in the database for"
                f"{instance.user}"
            )
