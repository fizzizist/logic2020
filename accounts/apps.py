from django.apps import AppConfig


class AccountsConfig(AppConfig):
    """Configuration class for accounts app."""

    name = "accounts"

    def ready(self):
        import accounts.signals  # noqa
