FROM python:3.12-alpine as dev

MAINTAINER Fizzizist "pvlasveld@protonmail.com"

RUN apk update \
  && apk add npm

COPY ./requirements/dev.txt /app/requirements/dev.txt
RUN pip install --upgrade pip \
  && pip install pip-tools \
  && pip-sync /app/requirements/dev.txt

COPY . /app

WORKDIR /app

RUN npm install
RUN npm run build

FROM python:3.12-alpine as test

MAINTAINER Fizzizist "pvlasveld@protonmail.com"

COPY ./requirements/dev.txt /app/requirements/dev.txt
RUN pip install --upgrade pip \
  && pip install -r /app/requirements/dev.txt

WORKDIR /app

ENTRYPOINT ["pytest"]
