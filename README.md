[![pipeline status](https://gitlab.com/fizzizist/logic2020/badges/master/pipeline.svg)](https://gitlab.com/fizzizist/logic2020/commits/master)
[![Python 3.12](https://img.shields.io/badge/python-3.12-blue)](https://www.python.org/downloads/release/python-370/)
[![codecov](https://codecov.io/gl/fizzizist/logic2020/branch/master/graph/badge.svg)](https://codecov.io/gl/fizzizist/logic2020)
# Logic2020

Logic2020 is a Django/React project meant to be an educational tool for students studying Formal Symbolic Logic.
It is based off of Logic 2010, an educational application that is adopted by many Universities in the United States and Canada.
Logic 2020 is meant to be an updated, web-based version of Logic2010, allowing students to study logic from anywhere, even from their phone browser!

## Current Features

This application is currently in Beta.
Currently the user can create an account and is presented with a list of Sentential Derivations to solve. No tutorial is
given, just a series of buttons that the user can use to solve the derivation if they are already knowledgeable
about derivation syntax.
Currently the user can Show conclusions, Assume premises, and use Rules such as Modus Ponens and Modus Tolens and some other logical rules.

## Future Features ##

1.0 will likely have tutorials incorporated into it, with a several sets of sentential logic problems for the user to solve.
Moving past 1.0 will be the addition of more types of logical problems like more problem sets, Predicate Logic and Symbolization.

## Development

This app uses Docker for all things.

Provision your database:
```bash
docker compose run web python manage.py migrate
```

Spin up your dev server:
```bash
docker compose up -d --build
```

Run the backend test suite:
```bash
docker compose run web-test
```
You can add a more specific filepath to the above command to run specific tests.
