from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from loguru import logger

from logix.api.serializers import ArgumentSerializer
from logix.models import Argument
from logix.enums import ArgumentSet


class ImproperArgumentFormat(Exception):
    """Exception indicating that the argument is not in the proper format and
    shouldn't be added to the DB.
    """


class ArgumentViewSet(viewsets.ModelViewSet):
    """Rest API endpoint for arguments."""

    queryset = Argument.objects.all()
    serializer_class = ArgumentSerializer

    @staticmethod
    def _check_valid_symbol(*, symbol):
        """Checks that an atomic symbol is of the valid symbols available.
        :param symbol: str
        """
        possible_symbols = ["P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
        if symbol in possible_symbols:
            return True
        raise ImproperArgumentFormat

    def _check_atomic_type(self, *, premise):
        symbol = premise.get("symbol", None)
        if symbol:
            try:
                self._check_valid_symbol(symbol=symbol)
            except ImproperArgumentFormat:
                logger.error("Symbol is invalid.")
                raise
        else:
            raise ImproperArgumentFormat

    def _check_two_premise_validity(self, *, premise, premise_string1, premise_string2):
        premise1 = premise.get(premise_string1, None)
        premise2 = premise.get(premise_string2, None)
        if premise1 and premise2:
            try:
                self._check_premise_validity(premise=premise1)
                self._check_premise_validity(premise=premise2)
            except ImproperArgumentFormat:
                logger.error("Premise is invalid.")
                raise
        else:
            raise ImproperArgumentFormat

    def _check_not_premise_type(self, *, premise):
        premise1 = premise.get("premise", None)
        if premise1:
            try:
                self._check_premise_validity(premise=premise1)
            except ImproperArgumentFormat:
                logger.error("Premise is invalid.")
                raise
        else:
            raise ImproperArgumentFormat

    def _check_premise_validity(self, *, premise: dict):
        """Recursively checks through premise to make sure that each type has
        its respective parts.
        :param premise: dict
        """
        _type = premise.get("type", None)
        if _type:
            if _type == "atomic":
                try:
                    self._check_atomic_type(premise=premise)
                except ImproperArgumentFormat:
                    logger.error("Invalid atomic type.")
                    raise
            elif _type == "conditional":
                try:
                    self._check_two_premise_validity(
                        premise=premise,
                        premise_string1="antecedent",
                        premise_string2="consequent",
                    )
                except ImproperArgumentFormat:
                    logger.error("Not proper conditional format.")
                    raise
            elif _type in ["biconditional", "and", "or"]:
                try:
                    self._check_two_premise_validity(
                        premise=premise,
                        premise_string1="premise1",
                        premise_string2="premise2",
                    )
                except ImproperArgumentFormat:
                    logger.error("Not biconditional, and, or or format.")
                    raise
            elif _type == "not":
                try:
                    self._check_not_premise_type(premise=premise)
                except ImproperArgumentFormat:
                    logger.error('Bad "not" premise type.')
                    raise
            else:
                raise ImproperArgumentFormat
        else:
            raise ImproperArgumentFormat

    def _check_argument_validity(self, *, argument_data: dict):
        """Checks that the argument has a conclusion and a list of premises,
        and that those are valid.
        :param argument_data: dict
        """
        if argument_data:
            premises = argument_data.get("premises", None)
            conclusion = argument_data.get("conclusion", None)
            if premises is not None and conclusion:
                if len(premises) > 0:
                    for premise in premises:
                        try:
                            self._check_premise_validity(premise=premise)
                        except ImproperArgumentFormat:
                            logger.error("Premises were not formatted " "properly")
                            raise
                try:
                    self._check_premise_validity(premise=conclusion)
                except ImproperArgumentFormat:
                    logger.error("Conclusion was not formatted properly")
                    raise
            else:
                logger.error("Premises or conclusion were missing from " "argument.")
                raise ImproperArgumentFormat
        else:
            logger.error("Argument dict itself was missing.")
            raise ImproperArgumentFormat

    def create(self, request, *args, **kwargs):
        """Runs when a new argument is added to the database. Checks to make
        sure the argument is
        valid before adding it.
        """
        data = request.data.copy()
        if (
            int(data["argument_set"]) == int(ArgumentSet.CUSTOM)
            or request.user.is_superuser
        ):
            try:
                self._check_argument_validity(argument_data=data.get("argument", None))
            except ImproperArgumentFormat:
                logger.error("The input JSON was not in the proper format.")
                return Response(data, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

            serializer = self.serializer_class(context={request: request}, data=data)
            try:
                serializer.is_valid(raise_exception=True)
                serializer.save()

            except ValidationError as err:
                logger.error(err)
                return Response(data, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
            headers = self.get_success_headers(serializer.data)
            return Response(
                serializer.data, status=status.HTTP_202_ACCEPTED, headers=headers
            )

        logger.error(
            "User must be superuser in order to add arguments that are not custom."
        )
        return Response(data, status=status.HTTP_401_UNAUTHORIZED)
