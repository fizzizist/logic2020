from rest_framework import viewsets
from logix.api.serializers import DerivationSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response


class DerivationViewSet(viewsets.ModelViewSet):
    """Contains the API logic for the /derivations/ endpoints."""

    serializer_class = DerivationSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return self.request.user.derivations.all()

    def create(self, request):
        request.data["user"] = request.user.id
        s = self.get_serializer(data=request.data)
        s.is_valid(raise_exception=True)
        s.save()
        return Response(s.data, status=201)
