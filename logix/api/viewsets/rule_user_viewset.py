from rest_framework import viewsets
from rest_framework.response import Response

from logix.api.serializers import RuleUserSerializer
from logix.models import RuleUser


class RuleUserViewset(viewsets.ModelViewSet):
    """Rest API endpoint for rule-user information."""
    queryset = RuleUser.objects.all()
    serializer_class = RuleUserSerializer

    def get_queryset(self):
        rule_label = self.request.query_params.get('label', None)
        if self.request.query_params.get('unlockedonly', None):
            return self.queryset.filter(user=self.request.user, unlocked=True)
        if rule_label:
            return self.queryset.filter(user=self.request.user, rule__label=rule_label)
        return self.queryset.filter(user=self.request.user)

    def update(self, request, pk=None):
        data = request.data.copy()
        instance = self.get_object()
        serializer = self.serializer_class(instance=instance, data=data)

        if serializer.is_valid(raise_exception=True):
            serializer.save()

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, headers=headers)
