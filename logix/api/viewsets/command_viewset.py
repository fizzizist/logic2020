from rest_framework import viewsets
from logix.api.serializers import CommandSerializer
from rest_framework.permissions import IsAuthenticated


class CommandViewSet(viewsets.ModelViewSet):
    """Contains the API logic for the /derivations/<id>/commands endpoints.
    This will mostly be used for POSTing new commands.
    """

    serializer_class = CommandSerializer
    permission_classes = [IsAuthenticated]
