from rest_framework import serializers
from logix.models import Show


class ShowSerializer(serializers.ModelSerializer):
    """The serializer class for the ShowViewSet class"""

    class Meta:
        model = Show
        fields = ["id", "shown", "commands"]
