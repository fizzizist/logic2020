from rest_framework import serializers
from logix.models import Derivation


class DerivationSerializer(serializers.ModelSerializer):
    """The serializer class for the DerivationViewSet class"""

    class Meta:
        model = Derivation
        fields = ["id", "argument", "user", "solved", "show_conc"]
