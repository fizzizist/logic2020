from rest_framework import serializers

from logix.models import RuleUser


class RuleUserSerializer(serializers.ModelSerializer):
    """Serializer class for RuleUserViewset."""
    label = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    description = serializers.SerializerMethodField()
    command = serializers.SerializerMethodField()
    symbolization = serializers.SerializerMethodField()
    english = serializers.SerializerMethodField()
    unlocked_by = serializers.SerializerMethodField()

    class Meta:
        model = RuleUser
        fields = ['id', 'label', 'name', 'description', 'command', 'symbolization',
                  'english', 'unlocked_by', 'unlocked']

    def update(self, instance, validated_data):
        instance.unlocked = validated_data.get('unlocked', instance.unlocked)
        instance.save()
        return instance

    @staticmethod
    def get_label(obj: RuleUser) -> str:
        """Function to serialize Rule label.
        :param obj: RuleUser object to get Rule from.
        :return: str, containing label.
        """
        return obj.rule.label

    @staticmethod
    def get_name(obj: RuleUser) -> str:
        """Function to serialize Rule name.
        :param obj: RuleUser object to get Rule from.
        :return: str, containing name.
        """
        return obj.rule.name

    @staticmethod
    def get_description(obj: RuleUser) -> str:
        """Function to serialize Rule description.
        :param obj: RuleUser object to get Rule from.
        :return: str, containing description.
        """
        return obj.rule.description

    @staticmethod
    def get_unlocked_by(obj: RuleUser) -> str or None:
        """Function to serialize Rule unlocked_by string.
        :param obj: RuleUser object to get Rule from.
        :return: str or None, containing unlocked_by string if it exists.
        """
        return obj.rule.unlocked_by

    @staticmethod
    def get_command(obj: RuleUser) -> str:
        """Function to serialize Rule command.
        :param obj: RuleUser object to get Rule from.
        :return: str, containing command.
        """
        return obj.rule.command

    @staticmethod
    def get_symbolization(obj: RuleUser) -> str or None:
        """Function to serialize Rule symbolization.
        :param obj: RuleUser object to get Rule from.
        :return: str or None, symbolization of rule if it exists.
        """
        return obj.rule.symbolization

    @staticmethod
    def get_english(obj: RuleUser) -> str or None:
        """Function to serialize Rule english argument.
        :param obj: RuleUser object to get Rule from.
        :return: str or None, containing example english argument if it exists.
        """
        return obj.rule.english
