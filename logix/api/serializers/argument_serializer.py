from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from logix.models import Argument


class ArgumentSerializer(serializers.ModelSerializer):
    """Serializer for ArgumentViewset."""
    argument = serializers.JSONField(validators=[UniqueValidator(
        queryset=Argument.objects.all())])

    class Meta:
        model = Argument
        fields = '__all__'

    def create(self, validated_data) -> Argument:
        data = Argument.objects.create(**validated_data)
        return data
