from rest_framework import serializers
from logix.models import Command


class CommandSerializer(serializers.ModelSerializer):
    """The serializer class for the ShowViewSet class"""

    class Meta:
        model = Command
        fields = ["id", "child_show", "terms", "line_num"]
