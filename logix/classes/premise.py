from typing import Optional
from enum import Enum

# TODO tests and docs for this whole module
VALID_ATOMICS = {"P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}
JOINING_SYMBOLS = ["↔", "→", "v", "^"]


class PremiseType(Enum):
    ATOMIC = "atomic"
    CONDITIONAL = "conditional"
    BICONDITIONAL = "biconditional"
    AND = "and"
    OR = "or"
    NOT = "not"


class Premise:
    def __init__(
        self,
        premise_id: Optional[str] = None,
    ):
        self._id = premise_id
        if not self.is_valid():
            raise ValueError("Invalid Premise instantiation")

    def __eq__(self, premise):
        raise NotImplementedError(
            "This method was not implemented by the Premise class in use."
        )

    def is_valid(self):
        raise NotImplementedError(
            "This method was not implemented by the Premise class in use."
        )

    def __dict__(self):
        raise NotImplementedError(
            "This method was not implemented by the Premise class in use."
        )

    def __str__(self):
        raise NotImplementedError(
            "This method was not implemented by the Premise class in use."
        )

    @staticmethod
    def brackets_or_not(prem) -> str:
        if prem._type not in {PremiseType.ATOMIC, PremiseType.NOT}:
            return f"({prem})"
        return str(prem)


class BiconAndOr(Premise):
    def __init__(
        self,
        prem1: Premise,
        prem2: Premise,
        premise_id: Optional[str] = None,
    ):
        self.children = [prem1, prem2]
        super().__init__(premise_id)

    def __eq__(self, premise) -> bool:
        if not self._type == premise._type:
            return False

        if (
            self.children[0] == premise.children[0]
            and self.children[1] == premise.children[1]
        ):
            return True
        if (
            self.children[0] == premise.children[1]
            and self.children[1] == premise.children[0]
        ):
            return True
        return False

    def __dict__(self):
        return {
            "id": self._id,
            "type": self._type.value,
            "children": [dict(child) for child in self.children],
        }


class AndOr(BiconAndOr):
    def is_valid(self):
        return all([child.is_valid() for child in self.children])

    def __str__(self):
        child_strs = [self.brackets_or_not(child) for child in self.children]
        return f"{child_strs[0]} {self._op_symbol} {child_strs[1]}"


class And(AndOr):
    _type = PremiseType.AND
    _op_symbol = "^"


class Or(AndOr):
    _type = PremiseType.OR
    _op_symbol = "v"


class Biconditional(BiconAndOr):
    _type = PremiseType.BICONDITIONAL

    def __str__(self) -> str:
        child_strs = [self.brackets_or_not(child.antecedent) for child in self.children]
        return f"{child_strs[0]} ↔ {child_strs[1]}"

    def is_valid(self) -> bool:
        if not all([child._type == PremiseType.CONDITIONAL for child in self.children]):
            return False
        return (self.children[0].antecedent == self.children[1].consequent) and (
            self.children[1].antecedent == self.children[0].consequent
        )


class Conditional(Premise):
    _type = PremiseType.CONDITIONAL

    def __init__(
        self, antecedent: Premise, consequent: Premise, premise_id: Optional[str] = None
    ):
        self.antecedent = antecedent
        self.consequent = consequent
        super().__init__(premise_id)

    def is_valid(self) -> bool:
        return self.antecedent.is_valid() and self.consequent.is_valid()

    def __dict__(self) -> dict:
        return {
            "id": self._id,
            "type": self._type.value,
            "antecedent": dict(self.antecedent),
            "consequent": dict(self.consequent),
        }

    def __eq__(self, prem: Premise) -> bool:
        if not self._type == prem._type:
            return False
        return self.antecedent == prem.antecedent and self.consequent == prem.consequent

    def __str__(self) -> str:
        ante_str = self.brackets_or_not(self.antecedent)
        cons_str = self.brackets_or_not(self.consequent)
        return f"{ante_str} → {cons_str}"


class Not(Premise):
    _type = PremiseType.NOT

    def __init__(self, premise: Premise, premise_id: Optional[str] = None):
        self.premise = premise
        super().__init__(premise_id)

    def __str__(self):
        prem_str = self.brackets_or_not(self.premise)
        return f"~{prem_str}"

    def is_valid(self):
        return self.premise.is_valid()

    def __dict__(self) -> dict:
        return {"id": self._id, "type": self._type, "premise": dict(self.premise)}

    def __eq__(self, prem: Premise) -> bool:
        if self._type != prem._type:
            return False
        return self.premise == prem.premise


class Atomic(Premise):
    _type = PremiseType.ATOMIC

    def __init__(self, symbol: str, premise_id: Optional[str] = None):
        self.symbol = symbol
        super().__init__(premise_id)

    def __str__(self):
        return self.symbol

    def is_valid(self):
        return self.symbol in VALID_ATOMICS

    def __dict__(self) -> dict:
        return {"id": self._id, "type": self._type, "symbol": self.symbol}

    def __eq__(self, prem: Premise) -> bool:
        if self._type != prem._type:
            return False
        return self.symbol == prem.symbol


def _construct_obj_str(
    orig_str: str, unbrack: list, obj_str: list, symbol: str
) -> list:
    tmp = unbrack.split(symbol)
    tmp_obj_str = []

    if "()" not in tmp[0]:
        tmp_obj_str.append(tmp[0])
    elif tmp[0] == "()":
        tmp_obj_str.append(obj_str[0])
    elif tmp[0] == "~()":
        tmp_obj_str.append(f"~({obj_str[0]})")
    else:
        up_to_op = ""
        bracketed = 0
        done = False
        for c in orig_str:
            if c == "(":
                bracketed += 1
            if c == ")":
                bracketed -= 1
            if bracketed == 0 and c == symbol:
                done = True
            if not done:
                up_to_op += c
        tmp_obj_str.append(up_to_op)

    if "()" not in tmp[1]:
        tmp_obj_str.append(tmp[1])
    else:
        up_to_op = ""
        bracketed = 0
        not_yet = True
        for c in orig_str:
            if c == "(":
                bracketed += 1
            if c == ")":
                bracketed -= 1
            if not not_yet:
                up_to_op += c
            if bracketed == 0 and c == symbol:
                not_yet = False
        tmp_obj_str.append(up_to_op)

    return tmp_obj_str


def _get_object_str_unbracketed(prem_str: str) -> tuple[list[str], str]:
    object_str = []
    count = 0
    unbracketed = ""

    if "(" in prem_str:
        _object = ""
        for c in prem_str:
            if c == ")" and count == 1:
                object_str.append(_object)
                _object = ""
                count -= 1
                unbracketed += c
            elif count > 0 and c == ")":
                count -= 1
                _object += c
            elif c == "(" and count == 0:
                unbracketed += c
                count += 1
            elif count > 0 and c == "(":
                count += 1
                _object += c
            elif count > 0:
                _object += c
            elif count == 0:
                unbracketed += c

        if unbracketed == "()":
            return object_str, unbracketed

        for s in JOINING_SYMBOLS:
            if s in unbracketed:
                return _construct_obj_str(
                    prem_str, unbracketed, object_str, s
                ), unbracketed
    else:
        tokens = []
        for s in JOINING_SYMBOLS:
            if s in prem_str:
                tokens = prem_str.split(s)
                object_str.extend(tokens)
                unbracketed = s
                break

        if len(tokens) == 0 and "~" in prem_str:
            object_str.append(prem_str[-1])
            unbracketed = prem_str[:-1]

    return object_str, unbracketed


def premise_from_str(prem_str: str) -> Premise:
    """String parsing algorithm for getting a premise object from a premise string"""
    # atomic case
    if len(prem_str) == 1 and prem_str in VALID_ATOMICS:
        return Atomic(symbol=prem_str)

    object_str, unbracketed = _get_object_str_unbracketed(prem_str)

    # input was a single bracketed premise
    if unbracketed == "()":
        return premise_from_str(object_str[0])

    # negation case
    if len(object_str) == 1 and "~" in unbracketed:
        neg_count = unbracketed.count("~")
        if neg_count == 1 and ("(" in unbracketed or len(unbracketed) == 1):
            return Not(premise=premise_from_str(object_str[0]))
        elif neg_count > 1 and "(" in unbracketed:
            object_str[0] = f"({object_str[0]})"
            for i in range(neg_count - 1):
                object_str[0] = f"~{object_str[0]}"
            return Not(premise=premise_from_str(object_str[0]))
        else:
            for i in range(neg_count - 1):
                object_str[0] = f"~{object_str[0]}"
            return Not(premise=premise_from_str(object_str[0]))

    # conditional, biconditional, and, or cases
    # biconditional case
    if "↔" in unbracketed:
        cond_children = [premise_from_str(obj_str) for obj_str in object_str]
        children = [
            Conditional(antecedent=cond_children[0], consequent=cond_children[1]),
            Conditional(antecedent=cond_children[1], consequent=cond_children[0]),
        ]
        return Biconditional(prem1=children[0], prem2=children[1])
    # the rest
    for s in JOINING_SYMBOLS:
        if s in unbracketed:
            prem1 = premise_from_str(object_str[0])
            prem2 = premise_from_str(object_str[1])
            match s:
                case "→":
                    return Conditional(antecedent=prem1, consequent=prem2)
                case "^":
                    return And(prem1=prem1, prem2=prem2)
                case "v":
                    return Or(prem1=prem1, prem2=prem2)

    # this point should be unreachable with a valid input string
    raise ValueError(f"Invalid input premise string: {prem_str}")
