from django.shortcuts import render
from django.contrib.auth.decorators import login_required, user_passes_test


@login_required()
def index(request):
    """Derivation View"""
    return render(request, 'index.html')


@login_required()
def home_view(request):
    """View that the user immediately sees after login."""
    return render(request, 'home_logix.html')


@login_required()
@user_passes_test(lambda u: u.is_superuser)
def argument_constructor_admin(request):
    """View for admins to add arguments to the database"""
    return render(request, 'argumentConstructorAdmin.html')
