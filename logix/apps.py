from django.apps import AppConfig


class LogixConfig(AppConfig):
    """Configuration class for Logix app."""
    name = 'logix'
    default_auto_field = 'django.db.models.AutoField'
