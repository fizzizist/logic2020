import json

from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError

from loguru import logger

from logix.models import Argument, Rule


class Command(BaseCommand):
    """Management command to add argument set data to the DB."""

    help = (
        "Load an argument set into the database using one of the hard-coded json files."
    )

    def add_arguments(self, parser):
        """Function to add arguments to management command. Allowing the user to choose which
        argument set to fill.
        """
        parser.add_argument(
            "--argument_set",
            default=None,
            type=int,
            help="The argument set that you want to load (Can only do one at a "
            "time).",
        )

    def handle(self, *args, **options):
        """Function to actually run the command."""
        arg_set_num = options.get("argument_set")
        if arg_set_num is None:
            logger.error("No argument set specified")
            return
        if arg_set_num not in [1, 2, 3, 4, 5, 6]:
            logger.error(
                "Currently Argument sets 1 through 6 are the only sets that can be "
                "loaded."
            )
            return
        filepath = f"logix/management/argset{arg_set_num}.json"
        with open(filepath) as json_file:
            data = json.load(json_file)

        # Add arguments to database
        logger.info("bulk creating arguments..")
        rule_id_index = [
            Rule.objects.get(label=item["rule"]).id
            if item.get("rule", None) is not None
            else None
            for item in data
        ]
        arguments = [
            Argument(
                argument=item["argument_json"]["argument"],
                argument_set=arg_set_num,
                rule_id=rule_id_index[i],
            )
            for i, item in enumerate(data)
        ]
        try:
            Argument.objects.bulk_create(arguments)
        except IntegrityError as err:
            logger.error(
                "Looks like one or more of these arguments already exists in the "
                "database."
            )
            logger.error(err)
            return
