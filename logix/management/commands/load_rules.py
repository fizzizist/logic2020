from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from loguru import logger
from tqdm import tqdm

from logix.models import Rule, RuleUser


class Command(BaseCommand):
    """Management command to fill Rule and RuleUser data into the database."""
    help = 'Load Rule names and descriptions into Rule Model and create ' \
           'User data as well.'

    rules = {
        'always_unlocked': [
            {'label': 'R',
             'name': 'Repetition',
             'description': 'Repetition states that if a premise is true, then the same premise'
                            'is true if repeated.',
             'english': 'The cat is black (premise). Therefore, the cat is black (conclusion).',
             'symbolization': 'P ∴ P',
             'command': '[Premise] R'},
            {'label': 'MP',
             'name': 'Modus Ponens',
             'description': 'Modus Ponens states that the consequent of a conditional premise is '
                            'true if the antecedent of that premise is also true.',
             'english': 'If it is raining outside then the ground is wet (conditional premise). '
                        'It is raining outside (antecedent to previous sentence). '
                        'Therefore the ground is wet (conclusion).',
             'symbolization': 'P→Q. P ∴ Q',
             'command': '[Premise] [Premise] MP'},
            {'label': 'MT',
             'name': 'Modus Tollens',
             'description': 'Modus Tollens states that the negation of the antecedent of a '
                            'conditional premise is true if the negation of its consequent is also'
                            'true.',
             'english': 'If it is raining outside then the ground is wet (conditional premise). '
                        'The ground is not wet (negated consequent). '
                        'Therefore, it is not raining outside (conclusion).',
             'symbolization': 'P→Q. ~Q ∴ ~P',
             'command': '[Premise] [Premise] MT'},
            {'label': 'DN',
             'name': 'Double Negation',
             'description': 'The double negation is a parent rule that is '
                            'comprised of two rules: '
                            'Double Negation Elimination (DNE), and Double '
                            'Negation Insertion '
                            '(DNI).',
             'command': '[Premise] DN'},
            {'label': 'DNE',
             'name': 'Double Negation Elimination',
             'description': 'The double negation elimination rule states that '
                            'if a premise that is negated twice is true, then its underlying '
                            'premise is also true.',
             'english': 'It is not the case that it is not the case that the cat is '
                        'black (double negated premise). '
                        'Therefore the cat is black (conclusion).',
             'symbolization': '~~P ∴ P',
             'command': '[Premise] DNE'},
            {'label': 'DNI',
             'name': 'Double Negation Insertion',
             'description': 'The double negation insertion rule states that '
                            'if a premise is true, then if that premise is negated twice, it is '
                            'still true.',
             'english': 'The cat is black (premise). '
                        'Therefore, it is not the case that it is not the case that the cat '
                        'is black (conclusion).',
             'symbolization': 'P ∴ ~~P',
             'command': '[Premise] DNI'},
            {'label': 'S',
             'name': 'Simplification',
             'description': 'Simplification is a parent rule which contains '
                            'the two rules: '
                            'Simplification Left (SL) and Simplification '
                            'Right (SR).',
             'command': '[Premise] S'},
            {'label': 'SR',
             'name': 'Simplification Right',
             'description': 'The simplification right rule states that if a conjunction is true, '
                            'then the right premise of that conjunction must be true.',
             'english': 'The cat is black and the cat has eyes (conjunction of two premises). '
                        'Therefore, the cat has eyes (conclusion).',
             'symbolization': 'P^Q ∴ Q',
             'command': '[Premise] SR'},
            {'label': 'SL',
             'name': 'Simplification Left',
             'description': 'The simplification left rule states that if a conjunction is true, '
                            'then the left premise of that conjunction must be true.',
             'english': 'The cat is black and the cat has eyes (conjunction of two premises). '
                        'Therefore, the cat is black (conclusion).',
             'symbolization': 'P^Q ∴ P',
             'command': '[Premise] SL'},
            {'label': 'Adj',
             'name': 'Adjunction',
             'description': 'Adjunction states that if two premises are true on their own then the '
                            'conjunction of them must also be true.',
             'english': 'The cat is black (premise). '
                        'The cat has eyes (premise). '
                        'Therefore the cat is black and has eyes (conclusion).',
             'symbolization': 'P. Q ∴ P^Q',
             'command': '[Premise] [Premise] Adj'},
            {'label': 'AddR',
             'name': 'Addition Right',
             'description': 'The addition right rule states that if a premise is true, then a '
                            'disjunction with that premise on the right side of it must also be '
                            'true.',
             'english': 'The cat is black (premise). '
                        'Therefore, the cat is black or diamonds are red (conclusion).',
             'symbolization': 'P ∴ PvQ',
             'command': '[Premise] AddR'},
            {'label': 'Add',
             'name': 'Addition',
             'description': 'The addition rule is the exact same rule as '
                            'addition right. It is just a short form.',
             'command': '[Premise] Add'},
            {'label': 'AddL',
             'name': 'Addition Left',
             'description': 'The addition left rule states that if a premise is true, then a '
                            'disjunction with that premise on the left side of it must also be '
                            'true.',
             'english': 'The cat is black (premise). '
                        'Therefore, diamonds are red or the cat is black (conclusion).',
             'symbolization': 'P ∴ QvP',
             'command': '[Premise] AddL'},
            {'label': 'MTP',
             'name': 'Modus Tollendo Ponens',
             'description': 'Modus Tollendo Ponens states that if a disjunction is true and one of'
                            'its premises is false, then the other premise must be true.',
             'english': 'The cat is not black (negated premise). '
                        'Either the sky is blue, or the cat is black (disjunction). '
                        'Therefore the sky is blue (conclusion).',
             'symbolization': '~P. QvP ∴ Q',
             'command': '[Premise] [Premise] MTP'},
            {'label': 'BC',
             'name': 'Biconditional-Conditional',
             'description': 'The biconditional-conditional is a parent rule of the '
                            'biconditional-conditional right (BCR) and '
                            'biconditional-conditional left (BCL) rules.',
             'command': '[Premise] BC'},
            {'label': 'BCL',
             'name': 'Biconditional-Conditional Left',
             'description': 'The biconditional-conditional left rule states that if a '
                            'biconditional premise is true, then the underlying conditional with '
                            'the left premise as its antecedent must also be true.',
             'english': 'The sky is dark if and only if it is night-time (biconditional).'
                        ' Therefore the sky is dark only if it is night-time (conclusion).',
             'symbolization': 'P↔Q ∴ P→Q',
             'command': '[Premise] BCL'},
            {'label': 'BCR',
             'name': 'Biconditional-Conditional Right',
             'description': 'The biconditional-conditional right rule states that if a '
                            'biconditional premise is true, then the underlying conditional with '
                            'the right premise as its antecedent must also be true.',
             'english': 'The sky is dark if and only if it is night-time (biconditional).'
                        ' Therefore it is night-time only if the sky is dark (conclusion).',
             'symbolization': 'P↔Q ∴ Q→P',
             'command': '[Premise] BCR'},
            {'label': 'CB',
             'name': 'Conditional-Biconditional',
             'description': 'The conditional-biconditional rule states that if two conditional '
                            'premises that have the same underlying premises but work in opposing '
                            'directions are true, then a biconditional which contains those '
                            'premises must also be true.',
             'english': 'The sky is dark only if it is night-time (conditional). '
                        'It is night-time only if the sky id dark (conditional). '
                        'Therefore, the sky is dark if and only if it is night-time (conclusion).',
             'symbolization': 'P→Q. Q→P ∴ P↔Q',
             'command': '[Premise] [Premise] CB'}],
        'unlockable': [
            {'label': 'MC1',
             'name': 'Material Conditional 1',
             'description': 'The material conditional 1 rule states that if a premise is true then'
                            ' any conditional with that premise as its consequent must also be '
                            'true.',
             'english': 'The sky is blue (premise). '
                        'Therefore diamonds are red only if the sky is blue (conclusion).',
             'symbolization': 'P ∴ Q→P',
             'command': '[Premise] MC1',
             'unlocked_by': '"∴ P→(Q→P)" theorem in argument set #2.'},
            {'label': 'MC2',
             'name': 'Material Conditional 2',
             'description': 'The material conditional 2 rule states that if a premise is false, '
                            'then any conditional with that premise as the antecedent must be '
                            'true.',
             'english': 'The sky is not red (negated premise). '
                        'Therefore the sky is red only if diamonds are red (conclusion).',
             'symbolization': '~P ∴ P→Q',
             'command': '[Premise] MC2',
             'unlocked_by': '"∴ ~P→(P→Q)" theorem in argument set #2.'},
            {'label': 'MC',
             'name': 'Material Conditional',
             'description': 'The material conditional rule is a parent rule '
                            'of the material conditional 1 (MC1) and '
                            'material conditional 2 (MC2) rules.',
             'command': '[Premise] MC',
             'unlocked_by': 'unlocking both the MC1 and MC2 rules.'},
            {'label': 'COM1',
             'name': 'Commutativity 1',
             'description': 'Commutativity 1 states that if a conjunction is true, then a'
                            ' conjunction with the same premises flipped around is also true.',
             'english': 'It is raining and the ground is wet (conjunction). Therefore, the ground '
                        'is wet and it is raining (conclusion).',
             'symbolization': 'P^Q ∴ Q^P',
             'command': '[Premise] COM1',
             'unlocked_by': '"∴ (P^Q)↔(Q^P)" theorem in argument set #6'},
            {'label': 'COM2',
             'name': 'Commutativity 2',
             'description': 'Commutativity 2 states that if a disjunction is true, then a'
                            ' disjunction with the same premises flipped around is also true.',
             'english': 'It is raining or the ground is wet (disjunction). Therefore, the ground '
                        'is wet or it is raining (conclusion).',
             'symbolization': 'PvQ ∴ QvP',
             'command': '[Premise] COM2',
             'unlocked_by': '"∴ (PvQ)↔(QvP)" theorem in argument set #6'},
            # TODO: Add this rule when you add arg set 7.
            # {'label': 'COM3',
            #  'name': 'Commutativity 3',
            #  'description': 'Commutativity 3 states that if a biconditional is true, then a'
            #                 ' biconditional with the same premises flipped around is also true.',
            #  'english': 'It is raining if and only if the ground is wet (biconditional). '
            #             'Therefore, the ground is wet if and only if it is raining (conclusion).',
            #  'symbolization': 'P↔Q ∴ Q↔P',
            #  'command': '[Premise] COM1',
            #  'unlocked_by': '"∴ (P↔Q)↔(Q↔P)" theorem in argument set #7'
            #  }
            {'label': 'COM',
             'name': 'Commutativity',
             'description': 'The commutativity rule is a parent rule '
                            'of commutativity rules 1 through 3 (COM1, COM2, and COM3).',
             'command': '[Premise] COM',
             'unlocked_by': 'unlocking COM1, COM2, and COM3 rules.'},
            {'label': 'ASSOC1',
             'name': 'Associativity 1',
             'description': 'Associativity 1 states that if a three-way nested conjunction is true,'
                            ' then another nested conjunction that contains the same premises is '
                            'also true.',
             'symbolization': 'P^(Q^R) ∴ (P^Q)^R',
             'command': '[Premise] ASSOC1',
             'unlocked_by': '"∴ (P^(Q^R))↔((P^Q)^R)" theorem in argument set #6'
             },
            {'label': 'ASSOC2',
             'name': 'Associativity 2',
             'description': 'Associativity 2 states that if a three-way nested conjunction is true,'
                            ' then another nested conjunction that contains the same premises is '
                            'also true.',
             'symbolization': '(P^Q)^R ∴ P^(Q^R)',
             'command': '[Premise] ASSOC2',
             'unlocked_by': '"∴ ((P^Q)^R)↔(P^(Q^R))" theorem in argument set #6'
             },
            {'label': 'ASSOC3',
             'name': 'Associativity 3',
             'description': 'Associativity 3 states that if a three-way nested disjunction is true,'
                            ' then another nested disjunction that contains the same premises is '
                            'also true.',
             'symbolization': 'Pv(QvR) ∴ (PvQ)vR',
             'command': '[Premise] ASSOC3',
             'unlocked_by': '"∴ (Pv(QvR))↔((PvQ)vR)" theorem in argument set #6'
             },
            {'label': 'ASSOC4',
             'name': 'Associativity 4',
             'description': 'Associativity 4 states that if a three-way nested disjunction is true,'
                            ' then another nested disjunction that contains the same premises is '
                            'also true.',
             'symbolization': 'Pv(QvR) ∴ (PvQ)vR',
             'command': '[Premise] ASSOC4',
             'unlocked_by': '"∴ (Pv(QvR))↔((PvQ)vR)" theorem in argument set #6'
             },
            # TODO: arg set 7.
            # {'label': 'ASSOC5',
            #  'name': 'Associativity 5',
            #  'description': 'Associativity 5 states that if a three-way nested biconditional is '
            #                 'true, then another nested biconditional that contains the same '
            #                 'premises is also true.',
            #  'symbolization': 'P↔(Q↔R) ∴ (P↔Q)↔R',
            #  'command': '[Premise] ASSOC5',
            #  'unlocked_by': '"∴ (P↔(Q↔R))↔((P↔Q)↔R)" theorem in argument set #7'
            #  },
            # {'label': 'ASSOC6',
            #  'name': 'Associativity 6',
            #  'symbolization': 'P↔(Q↔R) ∴ (P↔Q)↔R',
            #  'command': '[Premise] ASSOC6',
            #  'unlocked_by': '"∴ (P↔(Q↔R))↔((P↔Q)↔R)" theorem in argument set #7'
            #  },
            {'label': 'ASSOC',
             'name': 'Associativity',
             'description': 'The associativity rule is a parent rule '
                            'of associativity rules 1 through 6 (ASSOC1, ASSOC2, ASSOC3, ASSOC4, '
                            'ASSOC5 and ASSOC6).',
             'command': '[Premise] ASSOC',
             'unlocked_by': 'unlocking ASSOC1, ASSOC2, ASSOC3, ASSOC4, ASSOC5 and ASSOC6 rules.'},
            {'label': 'EXP',
             'name': 'Exportation/Importation',
             'english': 'The sky is dark and the ground is wet only if it is raining (conditional '
                        'with conjunction antecedent). Therefore, the sky is dark only if, the '
                        'ground is wet only if it is raining (conclusion). The reverse is also '
                        'true.',
             'symbolization': 'P^Q→R ∴ P→(Q→R)',
             'command': '[Premise] EXP',
             'unlocked_by': '"∴ (P^Q→R)↔(P→(Q→R))" theorem in argument set #6'},
            {'label': 'SC1',
             'name': 'Separation of Cases 1',
             'description': 'Separation of cases 1 states that if a conditional is true and '
                            'another conditional with the opposite antecedent (negated) is true, '
                            'then the consequent of both must be true.',
             'english': 'If you work hard then you will get results (conditional). '
                        'If you do not work hard then you will get results (conditional with '
                        'opposite antecedent). Therefore you will get results (conclusion).',
             'symbolization': 'P→Q. ~P→Q ∴ Q',
             'command': '[Premise] [Premise] SC1',
             'unlocked_by': '"∴ ((P→Q)^(~P→Q))→Q" theorem in argument set #6'},
            {'label': 'SC2',
             'name': 'Separation of Cases 2',
             'english': 'Either you work hard or you do not work hard or you just work a little '
                        'bit (nested disjunction). '
                        'If you do not work hard then you will get results (conditional). '
                        'If you do work hard then you will get results (conditional). '
                        'If you just work a little bit, then you will get results (conditional). '
                        'Therefore you will get results (conclusion).',
             'symbolization': 'PvQvS. P→R. Q→R. S→R ∴ R',
             'command': '[3 or more Premises] SC2',
             'unlocked_by': '"∴ ((PvQ)^(P→R)^(Q→R))→R", "∴ ((Pv(QvS))^(P→R)^(Q→R)^(S→R))→R", and '
                            '"∴ ((Pv(Qv(SvT)))^(P→R)^(Q→R)^(S→R)^(T→R))→R" theorems in argument '
                            'set #6'},
            {'label': 'SC3',
             'name':  'Separation of Cases 3',
             'english': 'If you work hard, then you will get results (conditional).'
                        ' If you do not work hard, then you will get results(conditional with the '
                        'same consequent). Therefore if you work hard or if you do not work hard, '
                        'you will get results (conclusion).',
             'symbolization': 'P→R. Q→R ∴ (PvQ)→R',
             'command': '[Premise] [Premise] SC3',
             'unlocked_by': '"∴ ((P→R)^(Q→R))↔((PvQ)→R)" theorem in argument set #6.'},
            {'label': 'SC',
             'name': 'Separation of Cases',
             'description': 'Separation of Cases is a parent rule '
                            'of the SC1, SC2 and SC3 rules.',
             'command': '[2 or more Premises] SC',
             'unlocked_by': 'Unlocking SC1, SC2 and SC3 rules.'},
            {'label': 'NC',
             'name': 'Negation of a Conditional',
             'english': 'It is not the case that it is raining only if the sky if blue (negated '
                        'conditional). Therefore, it is raining and the sky is not blue '
                        '(conclusion). The reverse is also true.',
             'symbolization': '~(P→Q) ∴ P^~Q',
             'command': '[Premise] NC',
             'unlocked_by': '"∴ ~(P→Q)↔(P^~Q)" theorem in argument set #6.'},
            {'label': 'SSimp1',
             'name': 'Sentential Simplification 1',
             'english': 'It is raining outside (premise). It is snowing and it is raining outside '
                        'if and only if it is snowing outside (conclusion).',
             'symbolization': 'Q ∴ (P^Q)↔P',
             'command': '[Premise] SSimp1',
             'unlocked_by': '"∴ Q→((P^Q)↔P)" theorem in argument set #6.'},
            {'label': 'SSimp2',
             'name': 'Sentential Simplification 2',
             'english': 'It is raining outside (premise). It is raining outside and it is snowing '
                        'if and only if it is snowing outside (conclusion).',
             'symbolization': 'Q ∴ (Q^P)↔P',
             'command': '[Premise] SSimp2',
             'unlocked_by': '"∴ Q→((P^Q)↔P)" theorem in argument set #6.'},
            {'label': 'SSimp3',
             'name': 'Sentential Simplification 3',
             'english': 'It is not raining outside (premise). '
                        'It is raining outside or it is snowing if and only if it '
                        'is snowing outside (conclusion).',
             'symbolization': '~Q ∴ (PvQ)↔P',
             'command': '[Premise] SSimp3',
             'unlocked_by': '"∴ ~Q→((PvQ)↔P)" theorem in argument set #6.'},
            {'label': 'SSimp4',
             'name': 'Sentential Simplification 4',
             'english': 'It is not raining outside (premise). '
                        'It is raining outside or it is snowing if and only if it '
                        'is snowing outside (conclusion).',
             'symbolization': '~Q ∴ (QvP)↔P',
             'command': '[Premise] SSimp4',
             'unlocked_by': '"∴ ~Q→((PvQ)↔P)" theorem in argument set #6.'},
            {'label': 'SSimp5',
             'name': 'Sentential Simplification 5',
             'english': 'It is raining outside (premise). '
                        'It is raining outside only if it is snowing, if and only if it '
                        'is snowing outside (conclusion).',
             'symbolization': 'P ∴ (P→Q)↔P',
             'command': '[Premise] SSimp5',
             'unlocked_by': '"∴ P→(Q→P)" theorem in argument set #2.'},
            {'label': 'SSimp6',
             'name': 'Sentential Simplification 6',
             'english': 'It is not raining outside (premise). '
                        'It is raining outside only if it is snowing, if and only if it '
                        'is not snowing outside (conclusion).',
             'symbolization': '~Q ∴ (P→Q)↔~P',
             'command': '[Premise] SSimp6',
             'unlocked_by': '"∴ ~P→(P→Q)" theorem in argument set #2.'},
            # {'label': 'SSimp7',
            #  'name': 'Sentential Simplification 7',
            #  'english': 'It is raining outside (premise). '
            #             'It is raining outside if and only if it is snowing, if and only if it '
            #             'is snowing outside (conclusion).',
            #  'symbolization': 'P ∴ (P↔Q)↔Q',
            #  'command': '[Premise] SSimp7',
            #  'unlocked_by': '"∴ P^Q→(P↔Q)" theorem in argument set #7.'},
            # {'label': 'SSimp8',
            #  'name': 'Sentential Simplification 8',
            #  'english': 'It is snowing outside (premise). '
            #             'It is raining outside if and only if it is snowing, if and only if it '
            #             'is raining outside (conclusion).',
            #  'symbolization': 'Q ∴ (P↔Q)↔P',
            #  'command': '[Premise] SSimp8',
            #  'unlocked_by': '"∴ P^Q→(P↔Q)" theorem in argument set #7.'},
            # {'label': 'SSimp9',
            #  'name': 'Sentential Simplification 9',
            #  'english': 'It is not raining outside (premise). '
            #             'It is raining outside if and only if it is snowing, if and only if it '
            #             'is not snowing outside (conclusion).',
            #  'symbolization': '~Q ∴ (P↔Q)↔~P',
            #  'command': '[Premise] SSimp9',
            #  'unlocked_by': '"∴ ~P^~Q→(P↔Q)" theorem in argument set #7.'},
            # {'label': 'SSimp10',
            #  'name': 'Sentential Simplification 10',
            #  'english': 'It is not snowing outside (premise). '
            #             'It is raining outside if and only if it is snowing, if and only if it '
            #             'is not raining outside (conclusion).',
            #  'symbolization': '~P ∴ (P↔Q)↔~Q',
            #  'command': '[Premise] SSimp10',
            #  'unlocked_by': '"∴ ~P^~Q→(P↔Q)" theorem in argument set #7.'},
        ]}

    def handle(self, *args, **options):
        logger.info('Looping through always_unlocked rules and adding if '
                    'necessary...')
        added_count = 0
        rule_user_count = 0
        for rule in tqdm(self.rules['always_unlocked']):
            english = rule.get('english', None)
            symbolization = rule.get('symbolization', None)
            _, created = Rule.objects.get_or_create(
                label=rule['label'],
                name=rule['name'],
                description=rule['description'],
                command=rule['command'],
                english=english,
                symbolization=symbolization,
                unlocked_by=None,
            )
            if created:
                added_count += 1
        logger.info('Looping through unlockable rules and adding if '
                    'necessary...')
        for rule in tqdm(self.rules['unlockable']):
            english = rule.get('english', None)
            symbolization = rule.get('symbolization', None)
            _, created = Rule.objects.get_or_create(
                label=rule['label'],
                name=rule['name'],
                description=rule.get('description', None),
                command=rule['command'],
                english=english,
                symbolization=symbolization,
                unlocked_by=rule['unlocked_by'],
            )

            if created:
                added_count += 1
        logger.info('Adding appropriate user data...')
        users = User.objects.all()
        rules = Rule.objects.all()
        unlocked_labels = [i['label'] for i in self.rules['always_unlocked']]
        for user in tqdm(users):
            for rule in rules:
                if rule.label in unlocked_labels:
                    _, created = RuleUser.objects.get_or_create(user=user,
                                                                rule=rule,
                                                                unlocked=True)
                else:
                    _, created = RuleUser.objects.get_or_create(user=user,
                                                                rule=rule)
                if created:
                    rule_user_count += 1

        logger.info(f'{added_count} new rules were created along with '
                    f'{rule_user_count} new RuleUser rows.')
