"""Contains ArgumentSet Enum class."""
from django.db import models


class ArgumentSet(models.IntegerChoices):
    """Enum to hold argument sets."""
    CUSTOM = 0
    SET1 = 1
    SET2 = 2
    SET3 = 3
    SET4 = 4
    SET5 = 5
    SET6 = 6
