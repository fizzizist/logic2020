# Generated by Django 5.0 on 2024-01-01 22:23

import django.contrib.postgres.fields
import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('logix', '0002_auto_20211226_2159'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Show',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('shown', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Derivation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('solved', models.BooleanField(default=False)),
                ('argument', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='logix.argument')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('show_conc', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='logix.show')),
            ],
        ),
        migrations.CreateModel(
            name='Command',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('terms', django.contrib.postgres.fields.ArrayField(base_field=models.TextField(), size=None)),
                ('line_num', models.IntegerField()),
                ('derivation', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='logix.derivation')),
                ('child_show', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='parent_command', to='logix.show')),
                ('parent_show', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='commands', to='logix.show')),
            ],
        ),
        migrations.DeleteModel(
            name='ArgumentUser',
        ),
    ]
