from django.urls import path, include
from rest_framework_extensions.routers import ExtendedDefaultRouter
from logix.views import views
from logix.api.viewsets import (
    ArgumentViewSet,
    DerivationViewSet,
    RuleUserViewset,
    CommandViewSet,
)

ROUTER = ExtendedDefaultRouter()

ROUTER.register(r"arguments", ArgumentViewSet)
ROUTER.register(r"derivations", DerivationViewSet, basename="derivation").register(
    r"commands",
    CommandViewSet,
    parents_query_lookups=["derivation"],
    basename="command",
)
ROUTER.register(r"rules", RuleUserViewset)

urlpatterns = [
    # The home view might be useful in the future but for Beta it just makes sense to go right to
    # argument list page.
    # path('home/', views.home_view, name='home_logix'),
    path("", views.index, name="index"),
    path(
        "addarguments/",
        views.argument_constructor_admin,
        name="argument_constructor_admin",
    ),
    path("api/", include(ROUTER.urls)),
]
