import pytest
from django.contrib.auth.models import User
from logix.tests.test_models.model_factories import (
    UserFactory,
    RuleUserFactory,
    RuleFactory,
)
from logix.models import RuleUser


@pytest.mark.django_db
def test_mc1_cascade_mc():
    """Tests MC rule cascase for when MC2 becomes unlocked."""
    new_user = UserFactory()
    user = User.objects.get(id=new_user.id)
    rule_mc1 = RuleFactory(label="MC1")
    rule_mc2 = RuleFactory(label="MC2")
    rule_mc = RuleFactory(label="MC")
    RuleUserFactory(rule=rule_mc1, unlocked=True, user=user)
    rule_user_mc2 = RuleUserFactory(rule=rule_mc2, unlocked=False, user=user)
    RuleUserFactory(rule=rule_mc, unlocked=False, user=user)
    rule_user = RuleUser.objects.get(user=rule_user_mc2.user, rule__label="MC2")
    rule_user.unlocked = True
    rule_user.save()
    assert RuleUser.objects.get(rule=rule_mc, user=user).unlocked


@pytest.mark.django_db
def test_mc2_cascade_mc():
    """Tests MC cascade signal for when MC1 becomes unlocked."""
    new_user = UserFactory()
    user = User.objects.get(id=new_user.id)
    rule_mc1 = RuleFactory(label="MC1")
    rule_mc2 = RuleFactory(label="MC2")
    rule_mc = RuleFactory(label="MC")
    RuleUserFactory(rule=rule_mc2, unlocked=True, user=user)
    rule_user_mc1 = RuleUserFactory(rule=rule_mc1, unlocked=False, user=user)
    RuleUserFactory(rule=rule_mc, unlocked=False, user=user)
    rule_user = RuleUser.objects.get(user=rule_user_mc1.user, rule__label="MC1")
    rule_user.unlocked = True
    rule_user.save()
    assert RuleUser.objects.get(rule=rule_mc, user=user).unlocked
