import pytest
from logix.classes.premise import PremiseType, premise_from_str


@pytest.mark.parametrize(
    "prem_str,prem_str_rep,prem_type",
    [
        ("P→Q", "P → Q", PremiseType.CONDITIONAL),
        ("P↔Q", "P ↔ Q", PremiseType.BICONDITIONAL),
        ("PvQ", "P v Q", PremiseType.OR),
        ("P^Q", "P ^ Q", PremiseType.AND),
        ("~P", "~P", PremiseType.NOT),
        ("(P→Q)→R", "(P → Q) → R", PremiseType.CONDITIONAL),
        ("~P→Q", "~P → Q", PremiseType.CONDITIONAL),
        ("P^(R↔(~(QvS)→T))", "P ^ (R ↔ (~(Q v S) → T))", PremiseType.AND),
    ],
    ids=("cond", "bicond", "or", "and", "not", "nested_cond", "not_cond", "nested_and"),
)
def test_premise_from_str(prem_str, prem_str_rep, prem_type):
    prem = premise_from_str(prem_str)
    assert str(prem) == prem_str_rep
    assert prem._type == prem_type
