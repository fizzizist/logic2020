import pytest

from logix.models import Argument


@pytest.mark.django_db
def test_new_argument_post_success_arg_set_1(logged_in_super_client):
    """Tests POST request to add new argument."""
    argument_dict = {
        "argument": {
            "premises": [
                {"type": "not", "premise": {"type": "atomic", "symbol": "P"}},
                {
                    "type": "conditional",
                    "antecedent": {"type": "atomic", "symbol": "Q"},
                    "consequent": {"type": "atomic", "symbol": "R"},
                },
            ],
            "conclusion": {
                "type": "or",
                "premise1": {"type": "atomic", "symbol": "T"},
                "premise2": {"type": "atomic", "symbol": "S"},
            },
        },
        "argument_set": 1,
    }
    response = logged_in_super_client.post(
        "/api/arguments/", argument_dict, format="json"
    )
    assert response.status_code == 202
    assert Argument.objects.all().count() == 1


@pytest.mark.django_db
def test_new_argument_post_unauthorized(logged_in_client):
    """Tests POST request to add new argument from a user that is unauthorized to do so."""
    argument_dict = {
        "argument": {
            "premises": [
                {"type": "not", "premise": {"type": "atomic", "symbol": "P"}},
                {
                    "type": "conditional",
                    "antecedent": {"type": "atomic", "symbol": "Q"},
                    "consequent": {"type": "atomic", "symbol": "R"},
                },
            ],
            "conclusion": {
                "type": "or",
                "premise1": {"type": "atomic", "symbol": "T"},
                "premise2": {"type": "atomic", "symbol": "S"},
            },
        },
        "argument_set": 1,
    }
    response = logged_in_client.post("/api/arguments/", argument_dict, format="json")
    assert response.status_code == 401
    assert Argument.objects.all().count() == 0


@pytest.mark.django_db
def test_new_argument_post_success_arg_set_custom(logged_in_client):
    """Tests POST request to add new argument."""
    argument_dict = {
        "argument": {
            "premises": [
                {"type": "not", "premise": {"type": "atomic", "symbol": "P"}},
                {
                    "type": "conditional",
                    "antecedent": {"type": "atomic", "symbol": "Q"},
                    "consequent": {"type": "atomic", "symbol": "R"},
                },
            ],
            "conclusion": {
                "type": "or",
                "premise1": {"type": "atomic", "symbol": "T"},
                "premise2": {"type": "atomic", "symbol": "S"},
            },
        },
        "argument_set": 0,
    }
    response = logged_in_client.post("/api/arguments/", argument_dict, format="json")
    assert response.status_code == 202
    assert Argument.objects.all().count() == 1


@pytest.mark.django_db
def test_new_argument_post_invalid_symbol(logged_in_super_client):
    """Tests failed POST request for when there is an invalid symbol in an atomic Premise."""
    argument_dict = {
        "argument": {
            "premises": [
                {"type": "not", "premise": {"type": "atomic", "symbol": "A"}},
                {
                    "type": "conditional",
                    "antecedent": {"type": "atomic", "symbol": "Q"},
                    "consequent": {"type": "atomic", "symbol": "R"},
                },
            ],
            "conclusion": {
                "type": "or",
                "premise1": {"type": "atomic", "symbol": "T"},
                "premise2": {"type": "atomic", "symbol": "S"},
            },
        },
        "argument_set": 1,
    }
    response = logged_in_super_client.post(
        "/api/arguments/", argument_dict, format="json"
    )
    assert response.status_code == 422
    assert Argument.objects.all().count() == 0


@pytest.mark.django_db
def test_new_argument_post_missing_symbol(logged_in_super_client):
    """Tests failed POST request for when there is a missing symbol from an atomic."""
    argument_dict = {
        "argument": {
            "premises": [
                {"type": "not", "premise": {"type": "atomic"}},
                {
                    "type": "conditional",
                    "antecedent": {"type": "atomic", "symbol": "Q"},
                    "consequent": {"type": "atomic", "symbol": "R"},
                },
            ],
            "conclusion": {
                "type": "or",
                "premise1": {"type": "atomic", "symbol": "T"},
                "premise2": {"type": "atomic", "symbol": "S"},
            },
        },
        "argument_set": 1,
    }
    response = logged_in_super_client.post(
        "/api/arguments/", argument_dict, format="json"
    )
    assert response.status_code == 422
    assert Argument.objects.all().count() == 0


@pytest.mark.django_db
def test_new_argument_post_missing_consequent(logged_in_super_client):
    """Tests failed POST request for when a consequent is missing from a conditional."""
    argument_dict = {
        "argument": {
            "premises": [
                {"type": "not", "premise": {"type": "atomic", "symbol": "P"}},
                {
                    "type": "conditional",
                    "antecedent": {"type": "atomic", "symbol": "Q"},
                },
            ],
            "conclusion": {
                "type": "or",
                "premise1": {"type": "atomic", "symbol": "T"},
                "premise2": {"type": "atomic", "symbol": "S"},
            },
        },
        "argument_set": 1,
    }
    response = logged_in_super_client.post(
        "/api/arguments/", argument_dict, format="json"
    )
    assert response.status_code == 422
    assert Argument.objects.all().count() == 0


@pytest.mark.django_db
def test_new_argument_post_missing_premise2(logged_in_super_client):
    """Tests failed POST request for when the second premise if missing from an OR."""
    argument_dict = {
        "argument": {
            "premises": [
                {"type": "not", "premise": {"type": "atomic", "symbol": "P"}},
                {
                    "type": "conditional",
                    "antecedent": {"type": "atomic", "symbol": "Q"},
                    "consequent": {"type": "atomic", "symbol": "R"},
                },
            ],
            "conclusion": {"type": "or", "premise1": {"type": "atomic", "symbol": "T"}},
        },
        "argument_set": 1,
    }
    response = logged_in_super_client.post(
        "/api/arguments/", argument_dict, format="json"
    )
    assert response.status_code == 422
    assert Argument.objects.all().count() == 0


@pytest.mark.django_db
def test_new_argument_post_missing_not_premise(logged_in_super_client):
    """Tests failed POST request for when there is a missing premise from NOT."""
    argument_dict = {
        "argument": {
            "premises": [
                {"type": "not"},
                {
                    "type": "conditional",
                    "antecedent": {"type": "atomic", "symbol": "Q"},
                    "consequent": {"type": "atomic", "symbol": "R"},
                },
            ],
            "conclusion": {
                "type": "or",
                "premise1": {"type": "atomic", "symbol": "T"},
                "premise2": {"type": "atomic", "symbol": "S"},
            },
        },
        "argument_set": 1,
    }
    response = logged_in_super_client.post(
        "/api/arguments/", argument_dict, format="json"
    )
    assert response.status_code == 422
    assert Argument.objects.all().count() == 0


@pytest.mark.django_db
def test_new_argument_post_bad_type(logged_in_super_client):
    """Tests failed POST for when there is a bad type."""
    argument_dict = {
        "argument": {
            "premises": [
                {"type": "bad_type", "premise": {"type": "atomic", "symbol": "P"}},
                {
                    "type": "conditional",
                    "antecedent": {"type": "atomic", "symbol": "Q"},
                    "consequent": {"type": "atomic", "symbol": "R"},
                },
            ],
            "conclusion": {
                "type": "or",
                "premise1": {"type": "atomic", "symbol": "T"},
                "premise2": {"type": "atomic", "symbol": "S"},
            },
        },
        "argument_set": 1,
    }
    response = logged_in_super_client.post(
        "/api/arguments/", argument_dict, format="json"
    )
    assert response.status_code == 422
    assert Argument.objects.all().count() == 0


@pytest.mark.django_db
def test_new_argument_post_no_type(logged_in_super_client):
    """Tests failed POST for when no type exists."""
    argument_dict = {
        "argument": {
            "premises": [
                {"type": "not", "premise": {"type": "atomic", "symbol": "P"}},
                {
                    "type": "conditional",
                    "antecedent": {"type": "atomic", "symbol": "Q"},
                    "consequent": {"type": "atomic", "symbol": "R"},
                },
            ],
            "conclusion": {
                "premise1": {"type": "atomic", "symbol": "T"},
                "premise2": {"type": "atomic", "symbol": "S"},
            },
        },
        "argument_set": 1,
    }
    response = logged_in_super_client.post(
        "/api/arguments/", argument_dict, format="json"
    )
    assert response.status_code == 422
    assert Argument.objects.all().count() == 0


@pytest.mark.django_db
def test_new_argument_post_missing_conclusion(logged_in_super_client):
    """Tests failed POST for when there is a missing conclusion."""
    argument_dict = {
        "argument": {
            "premises": [
                {"type": "not", "premise": {"type": "atomic", "symbol": "P"}},
                {
                    "type": "conditional",
                    "antecedent": {"type": "atomic", "symbol": "Q"},
                    "consequent": {"type": "atomic", "symbol": "R"},
                },
            ]
        },
        "argument_set": 1,
    }
    response = logged_in_super_client.post(
        "/api/arguments/", argument_dict, format="json"
    )
    assert response.status_code == 422
    assert Argument.objects.all().count() == 0


@pytest.mark.django_db
def test_new_argument_post_missing_argument(logged_in_super_client):
    """Tests failed POST for when there is a missing argument completely."""
    argument_dict = {"test": "test", "argument_set": 1}
    response = logged_in_super_client.post(
        "/api/arguments/", argument_dict, format="json"
    )
    assert response.status_code == 422
    assert Argument.objects.all().count() == 0
