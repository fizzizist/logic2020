import pytest
from logix.tests.test_models.model_factories import ArgumentFactory


@pytest.mark.django_db
def test_post_new_derivation(logged_in_client, client_setup):
    user = client_setup["user"]
    arg = ArgumentFactory()
    data = {"argument": arg.id}
    response = logged_in_client.post("/api/derivations/", data, format="json")

    assert response.status_code == 201
    ret_data = response.json()
    assert ret_data["solved"] is False
    assert ret_data["show_conc"] is None
    assert ret_data["user"] == user.id
    assert ret_data["argument"] == arg.id
