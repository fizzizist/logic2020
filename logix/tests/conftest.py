import pytest

from logix.tests.test_models.model_factories import UserFactory
from rest_framework.test import APIClient


@pytest.fixture
@pytest.mark.django_db
def client_setup(client):
    """Universal pytest ficture for setting up the client to make requests."""
    user = UserFactory(username="test1")
    return {"client": client, "user": user}


@pytest.fixture
@pytest.mark.django_db
def logged_in_client(client_setup):
    """Setup fixture for client."""
    client = APIClient()
    client.login(username="test1", password="1234fakepassword")
    client.force_authenticate(user=client_setup["user"])
    return client


@pytest.fixture
@pytest.mark.django_db
def logged_in_super_client(client_setup):
    """Setup fixture for client."""
    client = APIClient()
    client_setup["user"].is_superuser = True
    client_setup["user"].save()
    client.login(username="test1", password="1234fakepassword")
    client.force_authenticate(user=client_setup["user"])
    return client
