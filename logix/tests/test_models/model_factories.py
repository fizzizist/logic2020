from factory.django import DjangoModelFactory
from factory import Sequence, SubFactory
from django.contrib.auth.models import User
from logix.models import Argument, Rule, RuleUser, Derivation, Show, Command
from logix.enums import ArgumentSet


def fake_argument(num):
    """Sample argument dict."""
    return {
        f"{num}": {
            "premises": [{"id": "PR1", "type": "atomic", "symbol": "P"}],
            "conclusion": {"id": "C", "type": "atomic", "symbol": "Q"},
        }
    }


class ArgumentFactory(DjangoModelFactory):
    """Factory to create fake Argument objects."""

    class Meta:
        model = Argument

    argument = Sequence(fake_argument)
    argument_set = ArgumentSet.SET1
    rule = None


class UserFactory(DjangoModelFactory):
    """Factory to create fake User objects"""

    class Meta:
        model = User

    username = Sequence(lambda n: f"Test{n}")
    password = "1234fakepassword!"


class ShowFactory(DjangoModelFactory):
    """Factory to create fake Show objects."""

    class Meta:
        model = Show

    shown = False


class DerivationFactory(DjangoModelFactory):
    """Factory to create fake Derivation objects."""

    class Meta:
        model = Derivation

    user = SubFactory(UserFactory)
    argument = SubFactory(ArgumentFactory)


class CommandFactory(DjangoModelFactory):
    """Factory to create fake Command objects."""

    class Meta:
        model = Command

    parent_show = SubFactory(ShowFactory)
    terms = ["1", "2", "mp"]
    line_num = Sequence(lambda n: n)
    derivation = SubFactory(DerivationFactory)


class RuleFactory(DjangoModelFactory):
    """Factory for creating fake Rule objects."""

    class Meta:
        model = Rule

    label = Sequence(lambda n: f"Label{n}")
    name = Sequence(lambda n: f"Name{n}")
    description = Sequence(lambda n: f"Description{n}")
    command = Sequence(lambda n: f"Command{n}")


class RuleUserFactory(DjangoModelFactory):
    """Factory for creating fake RuleUser objects."""

    class Meta:
        model = RuleUser

    rule = SubFactory(RuleFactory)
    user = SubFactory(UserFactory)
