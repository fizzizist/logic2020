import pytest
from django.core.management import call_command
from logix.tests.test_models.model_factories import UserFactory
from logix.models import Rule, RuleUser


@pytest.mark.django_db
def test_load_rules():
    """Tests load_rules management command."""
    UserFactory()
    call_command('load_rules')
    assert Rule.objects.all().count() == 41
    assert RuleUser.objects.filter(unlocked=True).count() == 18
    assert RuleUser.objects.filter(unlocked=False).count() == 23
