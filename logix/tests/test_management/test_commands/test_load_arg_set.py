import pytest
from django.core.management import call_command
from logix.models import Argument


@pytest.mark.django_db
def test_load_arg_set_3():
    """Tests load_arg_set command for filling argument_set 3 with data."""
    call_command("load_arg_sets", argument_set=3)
    assert Argument.objects.all().count() == 13
