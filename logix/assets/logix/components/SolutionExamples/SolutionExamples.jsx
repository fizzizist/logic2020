// eslint-disable-next-line no-unused-vars
import React from 'react';
// eslint-disable-next-line no-unused-vars
import {Container, Accordion, Card} from 'react-bootstrap';
// eslint-disable-next-line no-unused-vars
import ReactPlayer from 'react-player';

/**
 * A react stateless component to hold example solution videos that are
 * hosted on YouTube
 * @param {*} props
 * @return {*}
 * @constructor
 */
const SolutionExamples = () => {
  const youtubeData = [
    {
      title: 'Direct Derivation',
      link: 'https://www.youtube.com/watch?v=_dB-S-B_8qo',
    },
    {
      title: 'Indirect Derivation',
      link: 'https://www.youtube.com/watch?v=dnGXjjE_oQE',
    },
    {
      title: 'Conditional Derivation',
      link: 'https://www.youtube.com/watch?v=zzDJvsLu6YY',
    },
    {
      title: 'Indirect Derivation (using custom Show commands)',
      link: 'https://www.youtube.com/watch?v=ESkKe8b7e1o',
    },
  ];

  const cards = [];
  youtubeData.forEach((pair, i) => {
    cards.push(
        <Card bg={'dark'} text={'white'}>
          <Accordion.Toggle
            as={Card.Header}
            style={{cursor: 'pointer'}}
            eventKey={`${i}`}
          >
            <b>{pair.title}</b>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey={`${i}`}>
            <ReactPlayer
              url={pair.link}
              controls
              width={'100%'}
            />
          </Accordion.Collapse>
        </Card>,
    );
  });

  return (
    <Container style={{marginTop: 10, marginBottom: 10}}>
      <Accordion >
        {cards}
      </Accordion>
    </Container>
  );
};

export default SolutionExamples;
