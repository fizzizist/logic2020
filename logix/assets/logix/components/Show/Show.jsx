// eslint-disable-next-line no-unused-vars
import React, {Component} from 'react';
// eslint-disable-next-line no-unused-vars
import {Table, Collapse} from 'react-bootstrap';
import uniqid from 'uniqid';
import PropTypes from 'prop-types';
// eslint-disable-next-line no-unused-vars
import InputController from '../InputController';

/**
 * React component that displays the output of a particular Show.
 */
class Show extends Component {
  /**
   * Constructor to the Show component.
   * @param {dict} props - dict passed in from parant Show or Derivation.
   */
  constructor(props) {
    super(props);
    this.props.conclusion.id = this.props.lastNumber.toString();
    this.toggleCollapsed = this.toggleCollapsed.bind(this);
    let lineInfo = [{
      lineNumber: this.props.lastNumber,
      output: `${this.props.conclusion.premiseString}`,
      command: '',
    }];
    let solved = false;
    if (props.lineInfo.length !== 0) {
      lineInfo = props.lineInfo;
      solved = true;
    }
    this.state = {
      lineInfo,
      childShow: false,
      outerPremises: props.outerPremises,
      linePremises: [],
      solved,
      lineNumber: props.lastNumber + 1,
      ownLineNumber: props.lastNumber,
      errorMessage: '',
      collapsed: {
        [props.lastNumber]: true,
      },
      currentSolution: props.currentSolution,
    };
    this.submitCommandCallback = this.submitCommandCallback.bind(this);
    this.newShow = this.newShow.bind(this);
    this.constructLines = this.constructLines.bind(this);
    this.solvedCallback = this.solvedCallback.bind(this);
    this.checkSolved = this.checkSolved.bind(this);
    this.restartShow = this.restartShow.bind(this);
  }

  /**
   * Function to return a new state given the incoming props.
   * @param {*} props - incoming props
   * @param {*} state - current state
   * @return {{lineInfo: *, currentSolution: *}} - new state.
   */
  static getDerivedStateFromProps(props, state) {
    if (props.currentSolution !== state.currentSolution) {
      return {
        currentSolution: props.currentSolution,
        lineInfo: props.lineInfo,
      };
    } else {
      return null;
    }
  }

  /**
   * In-built component method for after render.
   * @param {dict} prevProps - props from previous state.
   * @param {dict} prevState - previous state.
   */
  componentDidUpdate(prevProps, prevState) {
    if (prevState.errorMessage !== '') {
      this.setState({errorMessage: ''});
    }
    if (prevProps.restarting !== this.props.restarting) {
      this.restartShow(this.props.restartCallback);
    }
  }

  /**
   * Function to restart the show given a signal from above Derivation
   * component.
   * @param {function} restartCallback - callback function to the Derivation
   * component.
   */
  restartShow(restartCallback) {
    this.setState({
      lineInfo: [{
        lineNumber: this.props.lastNumber,
        output: `${this.props.conclusion.premiseString}`,
        command: '',
      }],
      childShow: false,
      outerPremises: this.props.outerPremises,
      linePremises: [],
      lineNumber: this.props.lastNumber + 1,
      ownLineNumber: this.props.lastNumber,
      collapsed: {
        [this.props.lastNumber]: true,
      },
      restarting: false,
    }, restartCallback);
  }

  /**
   * on-click function for collapsing the Show table.
   * @param {number} colIndex - index of the collapsed array.
   */
  toggleCollapsed(colIndex) {
    if (this.state.collapsed[colIndex]) {
      this.setState({
        collapsed: {...this.state.collapsed,
          [colIndex]: false},
      });
    } else {
      this.setState({
        collapsed: {...this.state.collapsed,
          [colIndex]: true},
      });
    }
  }

  /**
   * Callback function for InputController to submit a command to the main show
   * window.
   * @param {Premise} premise - Premise being passed out of the controller.
   */
  submitCommandCallback(premise) {
    const newLine = {
      lineNumber: this.state.lineNumber,
      output: premise.premiseString,
      command: premise.commandText,
    };
    this.setState(
        {
          lineInfo: [...this.state.lineInfo, newLine],
          linePremises: [...this.state.linePremises, premise],
          lineNumber: this.state.lineNumber + 1,
        },
        () => this.checkSolved(premise),
    );
  }

  /**
   * Callback function for set state in submitCommandCallback.
   * @param {Premise} premise - resulting premise.
   */
  checkSolved(premise) {
    const command = premise.commandText;
    const solvingRules = ['DD', 'ID', 'CD'];
    if (solvingRules.includes(command.substring(
        command.length - 2, command.length)) &&
        premise.equalsPremise(this.props.conclusion)) {
      const newLineNumber = parseInt(premise.id);
      premise.id = this.state.ownLineNumber.toString();
      const newLine = {
        lineNumber: this.props.lastNumber,
        output: `${this.props.conclusion.premiseString}`,
        command: 'strike',
      };
      this.setState(
          {
            lineInfo: [newLine, ...this.state.lineInfo.slice(1)],
            solved: true,
          },
          () => this.props.solved(
              premise,
              this.state.lineInfo,
              newLineNumber),
      );
    }
  }

  /**
   * callback function to instantiate a new child Show for this show.
   * @param {Premise} customPremise - The Premise to show from the new Show.
   */
  newShow(customPremise) {
    if (typeof customPremise === 'string') {
      this.setState({
        errorMessage: customPremise,
      });
    } else {
      this.setState((state) => ({
        childShow: true,
        childConclusion: customPremise,
        errorMessage: '',
      }));
    }
  }

  /**
   * Callback function to pass into child Show that gets triggered when that
   * show is solved.
   * @param {Premise} solvedConclusion - conclusion that got solved.
   * @param {Array} lineInfo - lineInfo passed from child Show.
   * @param {number} newLineNumber - to update line number properly
   */
  solvedCallback(solvedConclusion, lineInfo, newLineNumber) {
    this.setState((state) => ({
      lineInfo: [...this.state.lineInfo, lineInfo],
      linePremises: [...state.linePremises, solvedConclusion],
      lineNumber: newLineNumber + 1,
      childShow: false,
    }));
  }

  /**
   * Function for constructing lines to be displays with this show.
   * @param {Array} linesArr - nested array of elements.
   * @return {Array} - array of html elements.
   */
  constructLines(linesArr) {
    const lines = [];
    let showLine;
    if (linesArr[0].command === 'strike') {
      showLine = <span>{linesArr[0].lineNumber}: <s>Show</s> {
        linesArr[0].output}</span>;
    } else {
      showLine = <span>{linesArr[0].lineNumber}: Show {
        linesArr[0].output}</span>;
    }
    lines.push(
        <div
          key={uniqid()} onClick={() =>
            this.toggleCollapsed(linesArr[0].lineNumber)}
          aria-controls={`show-table-${linesArr[0].lineNumber}`}
          aria-expanded={this.state.collapsed[linesArr[0].lineNumber]}
          style={{cursor: 'pointer'}}
        >{showLine}</div>);
    linesArr.slice(1).forEach(function(line, _) {
      if (line instanceof Array) {
        const linesTable = this.constructLines(line);
        lines.push(<tr key={uniqid()}><td colSpan="3">{
          linesTable}</td></tr>);
      } else {
        const lineRow = <tr key={uniqid()}>
          <td>{line.lineNumber}</td>
          <td>{line.output}</td>
          <td>{line.command}</td>
        </tr>;
        lines.push(lineRow);
      }
    }.bind(this));
    const table = <Collapse
      key={uniqid()}
      in={this.state.collapsed[linesArr[0].lineNumber]}>
      <div
        key={uniqid()}
        id={`show-table-${linesArr[0].lineNumber}`}>
        <Table striped bordered hover>
          <tbody>
            {lines.slice(1)}
          </tbody>
        </Table>
      </div>
    </Collapse>;
    return [lines[0], table];
  }

  /**
   * The final HTML render from the Component.
   * @return {string} HTML containing all of the Component's elements.
   */
  render() {
    const linesTable = this.constructLines(this.state.lineInfo);
    let inputDisplay = {display: 'none'};
    if (!this.state.solved && !this.state.childShow) {
      inputDisplay = {display: 'inline'};
    }
    return (
      <div>
        <div id={`show-box-${this.state.ownLineNumber}`}>
          {linesTable}
        </div>
        {this.state.childShow &&
        <Show premises={this.props.premises}
          conclusion={this.state.childConclusion}
          lastNumber={this.state.lineNumber}
          outerPremises={this.state.linePremises.concat(
              this.state.outerPremises)}
          solved={this.solvedCallback}
          lineInfo={[]}
        />
        }
        <div style={inputDisplay}>
          <InputController
            premises={this.props.premises}
            conclusion={this.props.conclusion}
            submitCommand={this.submitCommandCallback}
            newShow={this.newShow}
            lineNumber={this.state.lineNumber}
            outerPremises={this.state.outerPremises}
            innerPremises={this.state.linePremises}
            restarting={this.props.restarting}/>
        </div>
        <p style={{color: 'red'}}>{this.state.errorMessage}</p>
      </div>
    );
  }
}

Show.propTypes = {
  lastNumber: PropTypes.number,
  solved: PropTypes.func,
  restarting: PropTypes.bool,
};

export default Show;
