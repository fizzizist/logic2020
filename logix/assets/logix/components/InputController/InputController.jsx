// eslint-disable-next-line no-unused-vars
import React, {Component} from 'react';
import uniqid from 'uniqid';
import axios from 'axios';
// eslint-disable-next-line no-unused-vars
import {Button, ButtonToolbar, Modal, Card, Alert, Form} from 'react-bootstrap';
import Rule from '../../classes/Rule';
import PremiseConstructor from '../../classes/PremiseConstructor';
import Premise from '../../classes/Premise';

/**
 * React Component for user input into the Derivation Component.
 */
class InputController extends Component {
  /**
   * Constructor for the Component.
   * @param {Array} props - Array of state variables from parent Component.
   */
  constructor(props) {
    super(props);
    this._isMounted = false;
    const dd = new Rule('DD');
    this.updateShowModal = this.updateShowModal.bind(this);
    const premiseConstructor = new PremiseConstructor(this.updateShowModal);
    this.state = {
      premises: this.props.premises,
      conclusion: this.props.conclusion,
      availablePremises: this.props.premises.concat(
          this.props.innerPremises).concat(this.props.outerPremises),
      selectedPremises: [],
      availableRules: [dd],
      unlockedRulesCount: 0,
      selectedRules: [],
      buttons: [],
      inputString: '',
      submitToggle: false,
      errorMessage: '',
      lineNumber: this.props.lineNumber,
      showShowMenuModal: false,
      showRuleModal: false,
      showRuleResultPrompt: false,
      selectedRule: null,
      premiseConstructor: premiseConstructor,
      showMenuString: '',
      assumeSubmitted: false,
      assumeSelected: false,
      assumeType: '',
    };
    this.constructButtons = this.constructButtons.bind(this);
    this.selectPremise = this.selectPremise.bind(this);
    this.assumeCD = this.assumeCD.bind(this);
    this.assumeID = this.assumeID.bind(this);
    this.showCons = this.showCons.bind(this);
    this.submitCommand = this.submitCommand.bind(this);
    this.toggleShowMenu = this.toggleShowMenu.bind(this);
    this.resetButtons = this.resetButtons.bind(this);
    this.reconstructRules = this.reconstructRules.bind(this);
    this.confirmNewRule = this.confirmNewRule.bind(this);
    this.toggleRuleMenu = this.toggleRuleMenu.bind(this);
    this.confirmRuleModal = this.confirmRuleModal.bind(this);
    this.toggleRuleResultPrompt = this.toggleRuleResultPrompt.bind(this);
    this.confirmRuleResultPrompt = this.confirmRuleResultPrompt.bind(this);
  }

  /**
   * Function that fires as soon as the component mounts.
   */
  componentDidMount() {
    this._isMounted = true;
    // Get unlocked rules for this user and add them to availableRules.
    this.fetchRuleArray((array) => {
      const rulesLength = array.length;
      array.push(this.state.availableRules[0]);
      if (this._isMounted) {
        this.setState({
          availableRules: array,
          unlockedRulesCount: rulesLength,
        });
      }
    });
  }

  /**
   * Static method to update state before render occurs.
   * @param {dict} props - new props.
   * @param {dict} state - previous state.
   * @return {{availablePremises: *, lineNumber: *}} - new state
   */
  static getDerivedStateFromProps(props, state) {
    return {
      availablePremises: props.premises.concat(
          props.innerPremises).concat(props.outerPremises),
      lineNumber: props.lineNumber,
    };
  }

  /**
   * In-built component method for after render.
   * @param {dict} prevProps - props from previous state.
   * @param {dict} prevState - previous state.
   */
  componentDidUpdate(prevProps, prevState) {
    if (prevState.errorMessage !== '') {
      this.setState({errorMessage: ''});
    }
    if (!prevProps.restarting && this.props.restarting) {
      this.restartController();
    }
  }

  /**
   * Function to be run when component is unmounting.
   */
  componentWillUnmount() {
    this._isMounted = false;
  }

  /**
   * Function to fetch unlocked rules from the database and return them as an
   * array.
   * @param {function} callback - callback function to pass array into.
   */
  fetchRuleArray(callback) {
    axios.get('/api/usersrules/?unlockedonly=1',
        {headers: {
          'X-CSRFToken': CSRF_TOKEN,
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }}).then(function(response) {
      const rules = response.data.map(function(rule) {
        return new Rule(rule.label);
      });
      callback(rules);
    }).catch(function(error) {
      console.log(error);
    });
  }

  /**
   * Function to restart the controller with the Show component when the user
   * presses the Restart button.
   */
  restartController() {
    const dd = new Rule('DD');
    this.fetchRuleArray((array) => {
      const rulesLength = array.length;
      array.push(dd);
      this.setState({
        premises: this.props.premises,
        conclusion: this.props.conclusion,
        availablePremises: this.props.premises.concat(
            this.props.innerPremises).concat(this.props.outerPremises),
        selectedPremises: [],
        availableRules: array,
        unlockedRulesCount: rulesLength,
        selectedRules: [],
        buttons: [],
        inputString: '',
        submitToggle: false,
        errorMessage: '',
        lineNumber: this.props.lineNumber,
        showShowMenuModal: false,
        showMenuString: '',
        assumeSubmitted: false,
        assumeSelected: false,
      });
    });
  }

  /**
   * Generates a button in the controller for each premise available to the
   * user.
   * @return {array} - Array of html button tags to be displayed in the
   * component.
   */
  constructButtons() {
    const buttons = [];
    // Add custom Show buttons
    const showCustomButton =
    <Button key={uniqid()} onClick={
      this.toggleShowMenu}>Show Custom</Button>;
    buttons.push(showCustomButton);
    if (this.state.conclusion.type === 'conditional') {
      const showConsButton =
      <Button key={uniqid()} onClick={
        this.showCons}>Show Cons</Button>;
      buttons.push(showConsButton);
    }

    // Add buttons for Assume statements.
    if (this.state.selectedPremises.length === 0) {
      if (this.state.conclusion.type === 'conditional' &&
          !this.state.assumeSelected && !this.state.assumeSubmitted) {
        const button = <Button
          className={'assume-button'}
          key={uniqid()}
          onClick={this.assumeCD}>Assume CD</Button>;
        buttons.push(button);
      }
      if (!this.state.assumeSelected && !this.state.assumeSubmitted) {
        const button = <Button
          className={'assume-button'}
          key={uniqid()}
          onClick={this.assumeID}>Assume ID</Button>;
        buttons.push(button);
      }
    }

    // Add Premise buttons for each Premise available to the user.
    this.state.availablePremises.forEach(function(premise, _) {
      const button = <Button variant={'dark'} key={uniqid()} onClick={
        () => this.selectPremise(premise)
      }>{premise.id}</Button>;
      buttons.push(button);
    }.bind(this));

    // Generate buttons for Rules
    this.state.availableRules.forEach(function(rule, _) {
      if (this.state.selectedPremises.length === rule.allowedPremises) {
        const button = <Button variant={'outline-dark'} key={uniqid()} onClick={
          () => this.selectRule(rule)
        }>{rule.name}</Button>;
        buttons.push(button);
      }
    }.bind(this));
    return buttons;
  }

  /**
   * On-click function for the Clear that resets the command box.
   */
  resetButtons() {
    this.setState({
      inputString: '',
      submitToggle: false,
      availablePremises: this.props.premises.concat(
          this.props.outerPremises).concat(
          this.props.innerPremises),
      selectedPremises: [],
      assumeSelected: false,
    },
    this.reconstructRules);
  }

  /**
   * Function to reconstruct availableRules to control certain conditional
   * rules.
   */
  reconstructRules() {
    const rules = this.state.availableRules.slice(
        0, this.state.unlockedRulesCount);
    if (this.state.assumeSelected || this.state.assumeSubmitted) {
      if (this.state.assumeType === 'CD') {
        const cdRule = new Rule('CD');
        rules.push(cdRule);
      } else if (this.state.assumeType === 'ID') {
        const idRule = new Rule('ID');
        rules.push(idRule);
      }
    } else {
      const ddRule = new Rule('DD');
      rules.push(ddRule);
    }
    this.setState((state) => ({
      availableRules: rules,
    }));
  }

  /**
   * on-click function for Assume CD button that allows the user to assume
   * the antecedent to the conditional conclusion.
   */
  assumeCD() {
    this.setState({
      selectedPremises: [...this.state.selectedPremises,
        this.state.conclusion.antecedent],
      inputString: this.state.inputString.concat('Ass CD'),
      submitToggle: true,
      assumeSelected: true,
      assumeType: 'CD',
    },
    this.reconstructRules);
  }

  /**
   * on-click function for Assume ID button that allows the user to assume
   * the negation of the conclusion in order to generate a contradiction.
   */
  assumeID() {
    const negatedConclusion = new Premise({
      type: 'not',
      premise1: this.state.conclusion,
    });
    this.setState({
      selectedPremises: [...this.state.selectedPremises, negatedConclusion],
      inputString: this.state.inputString.concat('Ass ID'),
      submitToggle: true,
      assumeSelected: true,
      assumeType: 'ID',
    },
    this.reconstructRules);
  }

  /**
   * On-Click function for Show Cons that allows the user to show the consequent
   * of the conditional conclusion in one click.
   */
  showCons() {
    this.props.newShow(this.state.conclusion.consequent);
  }

  /**
   * When a premise is clicked, it needs to be added to selectedPremises in
   * state.
   * @param {Premise} premise - The ID of the premise being selected.
   */
  selectPremise(premise) {
    this.setState((state) => ({
      selectedPremises: [...state.selectedPremises, premise],
      inputString: state.inputString.concat(' ', premise.id),
    }));
  }

  /**
   * On-click function for submit button of Rule Modal.
   */
  confirmRuleModal() {
    const newPremise = this.state.premiseConstructor.resultingPremise;
    const newRule = new Rule(
        this.state.selectedRule,
        this.state.selectedPremises[0],
        newPremise,
    );
    this.confirmNewRule(newRule);
    this.toggleRuleMenu();
  }

  /**
   * Function to assign resulting premise from the Rule Result modal.
   */
  confirmRuleResultPrompt() {
    const premise1Checked = document.getElementById(
        'resultingPremise1').checked;
    const premise2Checked = document.getElementById(
        'resultingPremise2').checked;
    if (premise1Checked) {
      const newPremise = this.state.selectedPremises[0].premise1;
      const newRule = new Rule(
          this.state.selectedRule,
          this.state.selectedPremises[0],
          newPremise,
      );
      this.confirmNewRule(newRule);
      this.toggleRuleResultPrompt();
    } else if (premise2Checked) {
      const newPremise = this.state.selectedPremises[0].premise2;
      const newRule = new Rule(
          this.state.selectedRule,
          this.state.selectedPremises[0],
          newPremise,
      );
      this.confirmNewRule(newRule);
      this.toggleRuleResultPrompt();
    } else {
      this.setState({
        errorMessage: 'You must choose an output for the rule.',
        showRuleResultPrompt: false,
      });
    }
  }

  /**
   * Function to report a bad premise to the user and set state accordingly.
   * @param {string} errorString - the string to display to the user.
   */
  reportBadRulePremise(errorString) {
    this.setState({
      availablePremises: this.props.premises.concat(
          this.props.innerPremises).concat(this.props.outerPremises),
      selectedPremises: [],
      inputString: '',
      errorMessage: errorString,
      submitToggle: false,
      assumeSelected: false,
    });
  }

  /**
   * Function to either confirm a new Rule or give an error to the user.
   * @param {Rule} newRule - New Rule resulting from premise rule select.
   */
  confirmNewRule(newRule) {
    const newPremise = newRule.resultingPremise;
    if (typeof newPremise === 'string') {
      this.reportBadRulePremise(newPremise);
    } else {
      const newDeepCopy = Object.assign(Object.create( Object.getPrototypeOf(
          newPremise)), newPremise);
      this.setState((state) => ({
        selectedPremises: [newDeepCopy],
        selectedRules: [...state.selectedRules, newRule],
        inputString: state.inputString.concat(' ', newRule.name),
        submitToggle: true,
      }));
    }
  }

  /**
   * On-click function for selecting a rule.
   * @param {Rule} rule - Rule object being selected.
   */
  selectRule(rule) {
    let newRule;
    if (rule.allowedPremises === 2) {
      switch (rule.name) {
        case 'ID':
          newRule = new Rule(
              rule.name,
              this.state.selectedPremises[0],
              this.state.selectedPremises[1],
              this.state.conclusion,
          );
          this.confirmNewRule(newRule);
          break;
        default:
          newRule = new Rule(
              rule.name,
              this.state.selectedPremises[0],
              this.state.selectedPremises[1],
          );
          this.confirmNewRule(newRule);
      }
    } else if (rule.allowedPremises === 1) {
      switch (rule.name) {
        case 'DD':
          newRule = new Rule(
              rule.name,
              this.state.selectedPremises[0],
              this.state.conclusion,
          );
          this.confirmNewRule(newRule);
          break;
        case 'CD':
          newRule = new Rule(
              rule.name,
              this.state.selectedPremises[0],
              this.state.conclusion.consequent,
              this.state.conclusion,
          );
          this.confirmNewRule(newRule);
          break;
        case 'Add':
        case 'AddR':
        case 'AddL':
        case 'MC1':
        case 'MC2':
        case 'MC':
          this.toggleRuleMenu(rule.name);
          break;
        case 'S':
          if (this.state.selectedPremises[0].type === 'and') {
            this.toggleRuleResultPrompt(rule.name);
          } else {
            this.reportBadRulePremise(
                'Cannot invoke S on a premise that is not a conjunction.');
          }
          break;
        case 'BC':
          if (this.state.selectedPremises[0].type === 'biconditional') {
            this.toggleRuleResultPrompt(rule.name);
          } else {
            this.reportBadRulePremise(
                'Cannot invoke BC on a premise that is not a biconditional.');
          }
          break;
        default:
          newRule = new Rule(
              rule.name,
              this.state.selectedPremises[0],
          );
          this.confirmNewRule(newRule);
          break;
      }
    }
  }

  /**
   * On-click function for the submit button that takes the resulting premise
   * and pushes it into the parent Show component.
   */
  submitCommand() {
    if (this.state.selectedPremises.length === 1) {
      let assumeBool = false;
      if (this.state.inputString.includes('Ass') ||
          this.state.assumeSubmitted) {
        assumeBool = true;
      }
      this.state.selectedPremises[0].id = this.state.lineNumber.toString();
      this.state.selectedPremises[0].commandText = this.state.inputString;
      this.props.submitCommand(this.state.selectedPremises[0]);
      this.setState((state) => ({
        inputString: '',
        submitToggle: false,
        availablePremises: this.props.premises.concat(
            this.props.outerPremises).concat(
            this.props.innerPremises),
        selectedPremises: [],
        lineNumber: state.lineNumber + 1,
        assumeSubmitted: assumeBool,
      }));
    }
  }

  /**
   * Toggle the rule modal that gets activated when certain rules are chosen.
   * @param {string} ruleName - optional name parameter to keep track of which
   * rule was selected.
   */
  toggleRuleMenu(ruleName = '') {
    if (this.state.showRuleModal) {
      this.setState({
        showRuleModal: false,
        showMenuString: '',
      });
      this.state.premiseConstructor.premiseString = '';
    } else {
      this.setState({
        showRuleModal: true,
        selectedRule: ruleName,
      });
    }
  }

  /**
   * Toggle for Show Modal that pops up to make a custom Show.
   */
  toggleShowMenu() {
    if (this.state.showShowMenuModal) {
      this.setState({
        showShowMenuModal: false,
        showMenuString: '',
      });
      this.state.premiseConstructor.premiseString = '';
    } else {
      this.setState({
        showShowMenuModal: true,
      });
    }
  }

  /**
   * Toggle for Modal that allows the user to choose which resulting premise
   * should result from certain rules such as S and BC.
   * @param {string} ruleName - name of the selected rule.
   */
  toggleRuleResultPrompt(ruleName = '') {
    this.setState({
      showRuleResultPrompt: !this.state.showRuleResultPrompt,
      selectedRule: ruleName,
    });
  }

  /**
   * Callback function passed into PremiseConstructor in order to update
   * this component's state when a button is pressed in the modal.
   */
  updateShowModal() {
    this.setState({
      showMenuString: this.state.premiseConstructor.premiseString,
    });
  }

  /**
   * Function to get the custom premise from the PremiseConstructor and feed
   * it to the new show.
   * @return {Premise} - Premise from PremiseConstructor.
   */
  showCustomPremise() {
    const newPremise = this.state.premiseConstructor.resultingPremise;
    this.toggleShowMenu();
    return newPremise;
  }

  /**
   * The final HTML render from the Component.
   * @return {string} HTML containing all of the Component's elements.
   */
  render() {
    const buttons = this.constructButtons();
    return (
      <div>
        <ButtonToolbar id={'input-controller-button-toolbar'}>
          {buttons}
        </ButtonToolbar>
        <Modal size='sm' show={this.state.showShowMenuModal} onHide={
          this.toggleShowMenu
        }>
          <Modal.Header>
            <Modal.Title>Show: {this.state.showMenuString}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <ButtonToolbar>
              {this.state.premiseConstructor.buttons}
            </ButtonToolbar><br/>
            <Button key={uniqid()} type="submit"
              onClick={() =>
                this.props.newShow(this.showCustomPremise())}>Submit</Button>
          </Modal.Body>
        </Modal>
        <Modal size='sm' show={this.state.showRuleModal} onHide={
          this.toggleRuleMenu
        }>
          <Modal.Header>
            <Modal.Title>New Premise</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Card>
              {this.state.showMenuString}
            </Card><br/>
            <ButtonToolbar>
              {this.state.premiseConstructor.buttons}
            </ButtonToolbar><br/>
            <Button key={uniqid()} type="submit"
              onClick={this.confirmRuleModal}>Submit</Button>
          </Modal.Body>
        </Modal>
        {this.state.selectedPremises.length > 0 &&
            this.state.selectedPremises[0].premise1 &&
            this.state.selectedPremises[0].premise2 &&
      <Modal size='sm' show={this.state.showRuleResultPrompt} onHide={
        this.toggleRuleResultPrompt
      }>
        <Modal.Header>
          <Modal.Title>New Premise</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group>
            <Form.Check
              type={'radio'}
              label={this.state.selectedPremises[0].premise1.premiseString}
              name={'rulePromptChecks'}
              id={'resultingPremise1'}
            />
            <Form.Check
              type={'radio'}
              label={this.state.selectedPremises[0].premise2.premiseString}
              name={'rulePromptChecks'}
              id={'resultingPremise2'}
            />
          </Form.Group>
          <Button
            key={uniqid()}
            type="submit"
            onClick={this.confirmRuleResultPrompt}
          >Submit</Button>
        </Modal.Body>
      </Modal>
        }
        <Card id={'command-card'}>
          <Card.Body>Command: {this.state.inputString}</Card.Body>
        </Card>
        <Button key={uniqid()} type='button'
          variant='secondary' onClick={
            this.resetButtons}>Clear</Button>
        {this.state.submitToggle &&
        <Button key={uniqid()} type='button' onClick={
          this.submitCommand}>Submit Command</Button>}
        {this.state.errorMessage &&
          <Alert variant={'danger'}>{this.state.errorMessage}</Alert>
        }
      </div>
    );
  }
}

export default InputController;
