// eslint-disable-next-line no-unused-vars
import React, {useState} from 'react';
// eslint-disable-next-line no-unused-vars
import {Container, Card, CardColumns, Collapse} from 'react-bootstrap';
import uniqid from 'uniqid';

const RuleInformation = (props) => {
  const cards = [];
  const collapseArray = [];
  const toggles = [];
  props.information.forEach((rule, i) => {
    const [collapse, setCollapse] = useState(false);
    toggles.push(() => setCollapse(!collapse));
    collapseArray.push([collapse, setCollapse]);
    cards.push(
        <Card key={uniqid()} bg={'dark'} text={'white'}>
          <Card.Header
            style={{cursor: 'pointer'}}
            onClick={toggles[i]}
            aria-controls={`card-body-${i}`}
            aria-expanded={collapseArray[i][0]}
          >{rule.name} ({rule.label})</Card.Header>
          <Collapse in={collapseArray[i][0]}>
            <div key={uniqid()} id={`card-body-${i}`}>
              <Card.Body>
                <Card.Text>
                  {rule.description &&
                  <span><b>Description:</b> {rule.description}<br/><br/></span>}
                  {rule.english &&
                  <span><b>In english:</b> {rule.english}<br/><br/></span>}
                  {rule.symbolization &&
                  <span><b>Symbolization:</b> {
                    rule.symbolization}<br/><br/></span>}
                  <span><b>Command:</b> {rule.command}<br/><br/></span>
                  {rule.unlocked_by &&
                  <span><b>Unlocked by</b> {rule.unlocked_by}<br/><br/></span>}
                </Card.Text>
              </Card.Body>
            </div>
          </Collapse>
        </Card>,
    );
  });

  return (
    <Container style={{marginTop: 10}}>
      <CardColumns>
        {cards}
      </CardColumns>
    </Container>
  );
};

export default RuleInformation;
