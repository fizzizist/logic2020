import React, {Component} from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import uniqid from 'uniqid';
// eslint-disable-next-line no-unused-vars
import {Button, ButtonToolbar, Form} from 'react-bootstrap';
// eslint-disable-next-line no-unused-vars
import Show from '../Show';
import ArgumentConstructor from '../../classes/ArgumentConstructor';

/**
 * React Component for the Derivation. This is basically a container for the
 * Show and InputController Components.
 */
class Derivation extends Component {
  /**
   * Constructor for the Component.
   * @param {array} props - Array of state variables from parent Component.
   */
  constructor(props) {
    super(props);
    const argumentConstructor = new ArgumentConstructor;
    const argument = argumentConstructor.getArgumentFromJSON(props.argument);
    let solved = false;
    let showing = false;
    let selectSolutions = false;
    let solutionsCount = 1;
    if (props.solutions.length !== 0) {
      solved = true;
      showing = true;
      selectSolutions = true;
      solutionsCount = props.solutions.length;
    }
    this.state = {
      showing,
      selectSolutions,
      premises: argument.premises,
      conclusion: argument.conclusion,
      solved,
      restarting: false,
      solutions: props.solutions,
      solutionsCount,
      currentSolution: 1,
    };
    this.inputCurrentSolution = React.createRef();
    this.showC = this.showC.bind(this);
    this.getDerivationPremiseString =
      this.getDerivationPremiseString.bind(this);
    this.solvedCallback = this.solvedCallback.bind(this);
    this.restartDerivation = this.restartDerivation.bind(this);
    this.restartCallback = this.restartCallback.bind(this);
    this.putArgumentSolution = this.putArgumentSolution.bind(this);
    this.changeCurrentSolution = this.changeCurrentSolution.bind(this);
    this.constructSolutionOptions = this.constructSolutionOptions.bind(this);
  }

  /**
   * Code to run as soon as the component first mounts.
   */
  componentDidMount() {
    if (this.props.joyrideRunning) {
      this.restartDerivation();
    }
  }

  /**
   * built-in react static function for deriving state from props before render.
   * @param {*} props - incoming props
   * @param {*} state - previous state
   * @return {*} - new state
   */
  static getDerivedStateFromProps(props, state) {
    if (props.joyrideRunning && props.joyrideShowSteps) {
      return {showing: true};
    } else {
      return null;
    }
  }

  /**
   * Toggles the show box for showing the conclusion.
   */
  showC() {
    this.setState({
      showing: true,
    });
  }

  /**
   * Function to concatenate all of the Premise strings into one for display.
   * @return {string} - String containing the premise' text.
   */
  getDerivationPremiseString() {
    let returnString = '';
    this.state.premises.forEach(function(premise) {
      returnString = returnString + `${premise.id}: ` +
      `${premise.premiseString} `;
    });
    return returnString;
  }

  /**
   * Function to add the new solution data into the database.
   */
  putArgumentSolution() {
    axios({
      method: 'PUT',
      url: `/api/usersarguments/${this.props.argumentID}/`,
      headers: {
        'X-CSRFToken': CSRF_TOKEN,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      data: {
        id: this.props.argumentID,
        argument_json: this.props.argument,
        solutions: this.state.solutions,
        solved: true,
      },
    })
        .then(function(response) {
          this.setState({
            solved: true,
            selectSolutions: true,
          });
        }.bind(this))
        .catch(function(error) {
          this.setState({
            errorMessage: error.response,
          });
        }.bind(this));
  }

  /**
   * Function to send an update request to the backend to unlock a specific
   * rule.
   * @param {string} ruleLabel - label of the rule to unlock.
   */
  unlockRule(ruleLabel) {
    axios({
      method: 'GET',
      url: `/api/usersrules/?label=${ruleLabel}`,
      headers: {
        'X-CSRFToken': CSRF_TOKEN,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    }).then(function(response) {
      if (!response.data[0].unlocked) {
        axios({
          method: 'PUT',
          url: `/api/usersrules/${response.data[0].id}/`,
          headers: {
            'X-CSRFToken': CSRF_TOKEN,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          data: {
            description: response.data[0].description,
            id: response.data[0].id,
            label: response.data[0].label,
            name: response.data[0].name,
            unlocked: true,
          },
        }).then(function(response) {
          alert(`You have unlocked the ${ruleLabel} rule!`);
        }).catch(function(error) {
          console.log(error);
        });
      }
    }).catch(function(error) {
      console.log(error);
    });
  }

  /**
   * Callback function to Show for when the derivation is finally solved.
   * @param {Premise} premise - Premise that was output from the Show.
   * @param {Array} lineInfo - lineInfo passed from child Show.
   * @param {number} newLineNumber - last line number form Show.
   */
  solvedCallback(premise, lineInfo, newLineNumber) {
    // Check for unlockables.
    if (premise.premiseString === 'Q → (P → Q)') {
      this.unlockRule('MC1');
    } else if (premise.premiseString === '~P → (P → Q)') {
      this.unlockRule('MC2');
    }
    // PUT new solution.
    this.setState({
      solutions: [...this.state.solutions, lineInfo],
    }, this.putArgumentSolution);
  }

  /**
   * on-click function for Restart button to restart the derivation from
   * scratch.
   */
  restartDerivation() {
    if (this.state.solved) {
      const newSolutionNum = this.state.solutionsCount + 1;
      this.setState({
        solutionsCount: newSolutionNum,
        currentSolution: newSolutionNum,
        selectSolutions: false,
        solved: false,
        showing: false,
      });
    } else {
      this.setState({
        restarting: true,
      });
    }
  }

  /**
   * callback function to indicate that the child show has finished restarting
   * and we can now turn off restarting.
   */
  restartCallback() {
    this.setState({
      restarting: false,
    });
  }

  /**
   * Function to construct the options for saved solution selection input box.
   * @return {Array} - options for saved solution selection.
   */
  constructSolutionOptions() {
    const options = [];
    for (let i = 1; i <= this.state.solutionsCount; i++) {
      options.push(<option key={uniqid()}>{i}</option>);
    }
    return options;
  }

  /**
   * Function that is called when user changes the saved solution selector.
   */
  changeCurrentSolution() {
    const selectedNum = this.inputCurrentSolution.current.value;
    this.setState({
      currentSolution: selectedNum,
    });
  }

  /**
   * The final HTML render from the Component.
   * @return {string} HTML containing all of the Component's elements.
   */
  render() {
    const solutionOptions = this.constructSolutionOptions();
    let lineInfo = [];
    if (this.state.solutions.length !== 0 && this.state.selectSolutions) {
      lineInfo = this.state.solutions[this.state.currentSolution - 1];
    }
    let derColor = {color: 'black'};
    if (this.state.solved) {
      derColor = {color: 'green'};
    }
    return (
      <div>
        {this.state.selectSolutions &&
        <Form.Group>
          <Form.Label>Solution #</Form.Label>
          <Form.Control
            value={this.state.currentSolution}
            as="select"
            ref={this.inputCurrentSolution}
            onChange={this.changeCurrentSolution}
          >
            {solutionOptions}
          </Form.Control>
        </Form.Group>
        }
        <p
          id={'argument-string'}
          style={derColor}
        >{this.getDerivationPremiseString()} &there4; {
            this.state.conclusion.premiseString}</p>
        {this.state.showing &&
          <React.Fragment>
            <Show
              lastNumber={1}
              conclusion={this.state.conclusion}
              premises={this.state.premises}
              solved={this.solvedCallback}
              outerPremises={[]}
              restarting={this.state.restarting}
              restartCallback={this.restartCallback}
              currentSolution={this.state.currentSolution}
              lineInfo={lineInfo}
            />
          </React.Fragment>
        }
        <ButtonToolbar>
          {!this.state.showing &&
            !this.state.solved &&
            <Button
              id={'show-conc-button'}
              type="button"
              onClick={this.showC}
            >Show Conc</Button>
          }
          {this.state.showing &&
          <Button type="button" onClick={
            this.restartDerivation}>Restart</Button>
          }
        </ButtonToolbar>
      </div>
    );
  }
}

Derivation.propTypes = {
  argumentID: PropTypes.number,
  argument: PropTypes.object,
};

export default Derivation;
