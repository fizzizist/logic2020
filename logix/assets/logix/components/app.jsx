import React, {Component} from 'react';
import uniqid from 'uniqid';
// eslint-disable-next-line no-unused-vars
import {Table, Button, Form, Modal, Col} from 'react-bootstrap';
import axios from 'axios';
// eslint-disable-next-line no-unused-vars
import Joyride, {STATUS, EVENTS, ACTIONS} from 'react-joyride';
import ArgumentConstructor from '../classes/ArgumentConstructor';
// eslint-disable-next-line no-unused-vars
import Derivation from './Derivation';
// eslint-disable-next-line no-unused-vars
import RuleInformation from './RuleInformation';
// eslint-disable-next-line no-unused-vars
import ArgumentConstructorApp from '../../argumentConstructor/components/app';
// eslint-disable-next-line no-unused-vars
import SolutionExamples from './SolutionExamples';

/**
 * React component for the main logix App window that lists problem sets.
 */
class App extends Component {
  /**
   * Constructor for main page of logix app.
   */
  constructor() {
    super();
    const argConstructor = new ArgumentConstructor();
    this.state = {
      runJoyride: false,
      joyrideStepIndex: 0,
      joyrideShowSteps: false,
      joyrideSteps: [
        {
          // 0
          content: (
            <div>
              <h1>Welcome to Logic2020</h1>
              <p>{'The following tutorial will familiarize you with the ' +
              'basic layout of the app, and how to open and begin solving' +
              ' derivations. For a more in-depth tutorial on the how to ' +
              'solve derivations, you can watch the instructional ' +
              'videos.'}</p>
            </div>),
          placement: 'center',
          target: 'body',
        },
        {
          // 1
          content: 'You can use the argument set selector to select ' +
          'from several sets of arguments to derive.',
          target: '#argument-set-selector',
        },
        {
          // 2
          content: 'The Rule Information button can be used to view ' +
              'information about all of the logical rules available ' +
              'to you when solving a derivation.',
          target: '#rule-information-button',
        },
        {
          // 3
          content: 'You can also add your own arguments by selecting ' +
              'the Custom argument set and pressing the Add Custom Argument ' +
              'button.',
          target: '#add-custom-argument-button',
        },
        {
          // 4
          content: 'Select an argument ' +
              'from the argument set to begin solving a derivation.',
          target: '#argument-list-table',
          placement: 'top',
          hideCloseButton: true,
          hideFooter: true,
          spotlightClicks: true,
          disableBeacon: true,
          disableOverlayClose: true,
          styles: {
            options: {
              zIndex: 10000,
            },
          },
        },
        {
          // 5
          content: 'This is the derivation screen where you can solve the' +
            ' derivation that you have just chosen.',
          placement: 'center',
          target: 'body',
          title: 'Derivations',
        },
        {
          // 6
          target: '#argument-string',
          content: 'This is the argument that you have chosen to solve. ' +
            'The premises are given labels so that you can use them in ' +
            'the derivation.',
        },
        {
          // 7
          content: 'To begin deriving you must first use the Show Conc ' +
            'command to indicate that you intend to show the conclusion.',
          target: '#show-conc-button',
        },
        {
          // 8
          content: 'After you invoke Show Conc, this button toolbar will ' +
            'change depending on your input, and allow for you to construct' +
            ' commands that can consist of any number of logical rules.',
          target: '#input-controller-button-toolbar',
        },
        {
          // 9
          content: 'The input commands will show up in this command box ' +
            'before submission',
          target: '#command-card',
        },
        {
          // 10
          content: 'If a command is successful, it will appear in this show' +
            'box with the resulting premise.',
          target: '#show-box-1',
        },
        {
          // 11
          content: 'To get an in-depth idea of how to solve a derivation, ' +
              'you can use the Solution Examples button to view a set of ' +
              'videos showing various ways to solve derivations.',
          target: '#solution-examples-button',
        },
        {
          // 12
          target: 'body',
          placement: 'center',
          content: 'That is all for this tour. To learn more about how to ' +
            'solve a derivation, watch the example solution videos by ' +
            'pressing the Example Solutions button.',
        },
      ],
      argumentArray: [],
      argumentConstructor: argConstructor,
      deriving: false,
      derivingArgument: null,
      derivingArgumentID: 0,
      derivingSolutions: null,
      ruleInformation: false,
      showSolutionExamples: false,
      argSet: 1,
      showArgumentConstructorApp: false,
    };
    this.inputArgSet = React.createRef();
    this.constructArgList = this.constructArgList.bind(this);
    this.deriveArgument = this.deriveArgument.bind(this);
    this.exitDerivation = this.exitDerivation.bind(this);
    this.fetchArgumentArray = this.fetchArgumentArray.bind(this);
    this.changeArgSet = this.changeArgSet.bind(this);
    this.toggleRuleInformation = this.toggleRuleInformation.bind(this);
    this.toggleArgumentConstructorApp =
        this.toggleArgumentConstructorApp.bind(this);
    this.activateJoyride = this.activateJoyride.bind(this);
    this.joyrideCallback = this.joyrideCallback.bind(this);
    this.toggleSolutionExamples = this.toggleSolutionExamples.bind(this);
  }

  /**
   * function to be run as soon as component mounts.
   */
  componentDidMount() {
    this.fetchArgumentArray();
  }

  /**
   * Post-Render function.
   * @param {array} prevProps - previous props.
   * @param {array} prevState - previous state.
   */
  componentDidUpdate(prevProps, prevState) {
    // custom hook for joyride tutorial.
    if (this.state.runJoyride
      && !prevState.deriving
      && this.state.deriving
      && this.state.joyrideStepIndex === 4) {
      this.setState({
        joyrideStepIndex: 5,
      });
    }
  }

  /**
   * Function to refetch argument array from backend.
   */
  fetchArgumentArray() {
    const argumentSet = this.state.argSet;
    axios({
      method: 'GET',
      url: `/api/usersarguments/?argumentset=${argumentSet}`,
      headers: {
        'X-CSRFToken': CSRF_TOKEN,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    })
        .then(function(response) {
          this.setState({argumentArray: response.data});
        }.bind(this))
        .catch(function(error) {
          console.log(error);
        });
  }

  /**
   * on-click function for table row in argument set view.
   * @param {JSON} argJSON - JSON of argument to be derived.
   * @param {number} argID - id of the argument.
   * @param {JSON} solutions - previous solutions (if any) for this argument.
   */
  deriveArgument(argJSON, argID, solutions) {
    if (solutions === null) {
      solutions = [];
    }
    this.setState({
      deriving: true,
      derivingArgument: argJSON.argument,
      derivingArgumentID: argID,
      derivingSolutions: solutions,
    });
  }

  /**
   * Static method to sort argument array by id.
   * @param {*} a - this item in argument array
   * @param {*} b - next item in argument array
   * @return {number}
   */
  static argumentSortCompare(a, b) {
    if (a.id > b.id) return 1;
    if (b.id > a.id) return -1;
    return 0;
  }

  /**
   * Function to construct a list of arguments for the user to choose from.
   * @return {Array} - list of arguments
   */
  constructArgList() {
    const argList = [];
    this.state.argumentArray.sort(App.argumentSortCompare).forEach(
        function(argument) {
          let solvedIcon;
          if (argument.solved) {
            solvedIcon = <i
              style={{
                color: '#b1ffad',
                fontSize: '48px',
                padding: 0,
                margin: 0,
              }}
              className={'fa fa-check'}
            />;
          } else {
            solvedIcon = <i
              style={{
                color: '#ffaead',
                fontSize: '48px',
                padding: 0,
                margin: 0,
              }}
              className={'fa fa-times'}
            />;
          }
          argList.push(
              <tr key={uniqid()} onClick={() => this.deriveArgument(
                  argument.argument_json, argument.id, argument.solutions)}
              style={{cursor: 'pointer'}}>
                <td>{argument.argument_json.argument_id}</td>
                <td>
                  {this.state.argumentConstructor.stringFromJSON(
                      argument.argument_json)}
                </td>
                <td
                  style={{textAlign: 'center', margin: 0, padding: 0}}
                >{solvedIcon}</td>
              </tr>,
          );
        }.bind(this));
    return argList;
  }

  /**
   * on-click function for exit button out of the derivation.
   */
  exitDerivation() {
    this.fetchArgumentArray();
    this.setState({
      deriving: false,
    });
  }

  /**
   * Function that is called when user changes the argument set selector.
   */
  changeArgSet() {
    this.setState({
      argSet: this.inputArgSet.current.value,
    }, this.fetchArgumentArray);
  }

  /**
   * On-click function to open the RuleInformation component.
   */
  toggleRuleInformation() {
    if (this.state.ruleInformation) {
      this.setState({
        ruleInformation: false,
      });
    } else {
      axios.get('/api/usersrules/',
          {
            headers: {
              'X-CSRFToken': CSRF_TOKEN,
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
          }).then((response) => {
        this.setState({
          ruleInformation: response.data,
        });
      }).catch((error) => {
        resolve(error);
      });
    }
  }

  /**
   * on-click function for solution examples component.
   */
  toggleSolutionExamples() {
    this.setState({
      showSolutionExamples: !this.state.showSolutionExamples,
    });
  }

  /**
   * on-click function to open argument constructor modal.
   */
  toggleArgumentConstructorApp() {
    if (!this.state.showArgumentConstructorApp) {
      this.setState({
        showArgumentConstructorApp: true,
      });
    } else {
      this.setState({
        showArgumentConstructorApp: false,
      });
      this.fetchArgumentArray();
    }
  }

  /**
   * on-click function to open joyride tutorial.
   */
  activateJoyride() {
    this.setState({
      runJoyride: true,
    });
  }

  /**
   * Callback function for joyride tour.
   * @param {array} data - data coming out of joyride component.
   */
  joyrideCallback(data) {
    const {action, index, type, status} = data;
    if ([STATUS.FINISHED, STATUS.SKIPPED].includes(status)) {
      this.setState({
        runJoyride: false,
        joyrideStepIndex: 0,
        deriving: false,
      });
    } else if ([EVENTS.STEP_AFTER, EVENTS.TARGET_NOT_FOUND].includes(type)) {
      const joyrideStepIndex = index + (action === ACTIONS.PREV ? -1 : 1);
      if (joyrideStepIndex === 3) {
        this.setState({
          argSet: '0',
          ruleInformation: false,
          joyrideStepIndex,
        }, this.fetchArgumentArray);
      } else if (joyrideStepIndex === 4) {
        this.setState({
          ruleInformation: false,
          argSet: '1',
          joyrideStepIndex,
        }, this.fetchArgumentArray);
      } else if (index === 4 && this.state.deriving) {
        setTimeout(() => {
          this.setState({
            runJoyride: true,
          });
        }, 400);
      } else if (joyrideStepIndex === 6 || joyrideStepIndex === 7) {
        this.setState({
          ruleInformation: false,
          deriving: true,
          joyrideStepIndex,
        });
      } else if (joyrideStepIndex === 8
          || joyrideStepIndex === 9
          || joyrideStepIndex === 10) {
        this.setState({
          ruleInformation: false,
          deriving: true,
          joyrideStepIndex,
          joyrideShowSteps: true,
        });
      } else {
        this.setState({
          ruleInformation: false,
          deriving: false,
          joyrideStepIndex,
        });
      }
    }
  }

  /**
   * The final HTML render from the Component.
   * @return {string} HTML containing all of the Component's elements.
   */
  render() {
    const argsList = this.constructArgList();
    return (
      <div style={{marginBottom: 25}}>
        <Joyride
          run={this.state.runJoyride}
          steps={this.state.joyrideSteps}
          callback={this.joyrideCallback}
          continuous
          showProgress
          showSkipButton
          stepIndex={this.state.joyrideStepIndex}
        />
        <h1 style={{fontFamily: 'Serif'}}>Logic 2020</h1>
        <Button
          id={'rule-information-button'}
          variant={'dark'}
          onClick={this.toggleRuleInformation}>Rule Information</Button>
        <Button
          id={'solution-examples-button'}
          variant={'dark'}
          onClick={this.toggleSolutionExamples}>Solution Examples</Button>
        <Button
          variant={'dark'}
          onClick={this.activateJoyride}>Take a tour</Button>
        {!this.state.deriving && !this.state.ruleInformation &&
            !this.state.showSolutionExamples &&
          <div>
            <Form.Group>
              <Form.Label>Argument Set</Form.Label>
              <Form.Row>
                <Col id={'argument-set-selector'}>
                  <Form.Control
                    value={this.state.argSet}
                    as="select"
                    ref={this.inputArgSet}
                    onChange={this.changeArgSet}
                  >
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option value={0}>Custom</option>
                  </Form.Control>
                </Col>
                <Col>
                  {this.state.argSet === '0' &&
                    <Button id={'add-custom-argument-button'}
                      onClick={this.toggleArgumentConstructorApp}
                      variant={'dark'}
                    >Add Custom Argument</Button>
                  }
                </Col>
              </Form.Row>
            </Form.Group>
            <Table
              id={'argument-list-table'}
              striped bordered hover variant="dark">
              <thead>
                <tr>
                  <th>ID #</th>
                  <th>Argument</th>
                  <th>Solved</th>
                </tr>
              </thead>
              <tbody>
                {argsList}
              </tbody>
            </Table>
            <Modal
              size="lg"
              show={this.state.showArgumentConstructorApp}
              onHide={this.toggleArgumentConstructorApp}
            >
              <Modal.Header>
                <Modal.Title>Construct new argument</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <ArgumentConstructorApp
                  isAdmin={false}
                  updateCallback={this.toggleArgumentConstructorApp}
                />
              </Modal.Body>
            </Modal>
          </div>

        }
        {this.state.deriving && !this.state.ruleInformation &&
            !this.state.showSolutionExamples &&
          <React.Fragment>
            <Derivation
              argument={this.state.derivingArgument}
              argumentID={this.state.derivingArgumentID}
              solutions={this.state.derivingSolutions}
              joyrideRunning={this.state.runJoyride}
              joyrideShowSteps={this.state.joyrideShowSteps}
            />
            <Button
              type="button"
              variant="dark"
              onClick={this.exitDerivation}
            >Exit</Button>
          </React.Fragment>
        }
        {this.state.ruleInformation &&
            <RuleInformation
              information={this.state.ruleInformation}
            />
        }
        {this.state.showSolutionExamples &&
          <SolutionExamples/>
        }
        <a href="/accounts/logout"><Button variant="dark">Logout</Button></a>
      </div>
    );
  }
}

export default App;
