import Premise from '../Premise';

/**
 * Generalized Rule class to represent all logical rules in the game for example
 * MP, MT, etc.
 */
class Rule {
  /**
   * constructor for Rule class.
   * @param {string} name - The name of the rule e.g. 'MP', 'MT'
   * @param {Premise} premise1 - The first premise analyzed by the rules.
   * @param {Premise} premise2 - The second premise if it exists.
   * @param {Premise} premise3 - third premise to hold the conclusion in the
   * case of a consitional derivation.
   */
  constructor(name, premise1=null, premise2=null, premise3=null) {
    this.name = name;
    switch (name) {
      case 'MP':
      case 'MT':
      case 'Adj':
      case 'MTP':
      case 'CB':
        if (premise1 && premise2) {
          this.premise1 = premise1;
          this.premise2 = premise2;
        }
        this.allowedPremises = 2;
        break;
      case 'Add':
      case 'AddR':
      case 'AddL':
      case 'MC1':
      case 'MC2':
      case 'MC':
      case 'S':
      case 'BC':
        if (premise1 && premise2) {
          this.premise1 = premise1;
          this.premise2 = premise2;
        }
        this.allowedPremises = 1;
        break;
      case 'ID':
        if (premise1 && premise2 && premise3) {
          this.premise1 = premise1;
          this.premise2 = premise2;
          this.conclusion = premise3;
        }
        this.allowedPremises = 2;
        break;
      case 'DD':
        if (premise1 && premise2) {
          this.premise = premise1;
          this.conclusion = premise2;
        }
        this.allowedPremises = 1;
        break;
      case 'CD':
        if (premise1 && premise2 && premise3) {
          this.premise = premise1;
          this.consequent = premise2;
          this.conclusion = premise3;
        }
        this.allowedPremises = 1;
        break;
      case 'DN':
      case 'DNE':
      case 'DNI':
      case 'SR':
      case 'SL':
      case 'BCR':
      case 'BCL':
      case 'R':
        if (premise1) {
          this.premise = premise1;
        }
        this.allowedPremises = 1;
        break;
    }
  }

  /**
   * Getter for the resulting premise of a rule.
   * Premises can be entered into the rule either way.
   * @return {Premise | string} - Premise object which is the result of the
   * rule or an error string explaining why it didn't work.
   */
  get resultingPremise() {
    switch (this.name) {
      // R: Repetition: just return the same premise again.
      case 'R':
        return this.premise;
      // MP: Modus Ponens: check which premise is the conditional and then see
      // if its antecedent is equal to the other premise.
      case 'MP':
        if (this.premise1 && this.premise2) {
          if (this.premise1.type === 'conditional' &&
              this.premise1.antecedent.equalsPremise(this.premise2)) {
            return this.premise1.consequent;
          } else if (this.premise2.type === 'conditional' &&
                  this.premise2.antecedent.equalsPremise(this.premise1)) {
            return this.premise2.consequent;
          } else {
            return 'Modus Ponens cannot be performed on these premises.';
          }
        } else {
          return 'The MP rule needs exactly two premises.';
        }

      // MT: Modus Tolens: check which premise is not and then see if its
      // inner negated premise is the same as the conditional consequent.
      // if so then return the negated conditional antecedent.
      case 'MT':
        if (this.premise1.type === 'not' &&
          this.premise2.type === 'conditional') {
          if (this.premise1.premise.equalsPremise(this.premise2.consequent)) {
            const newPremise = new Premise({
              type: 'not',
              premise1: this.premise2.antecedent,
            });
            return newPremise;
          } else {
            return 'Negated Premise does not match conditional consquent.';
          }
        } else if (this.premise2.type === 'not' &&
                   this.premise1.type === 'conditional') {
          if (this.premise2.premise.equalsPremise(this.premise1.consequent)) {
            const newPremise = new Premise({
              type: 'not',
              premise1: this.premise1.antecedent,
            });
            return newPremise;
          } else {
            return 'Negated Premise does not match conditional consquent.';
          }
        } else {
          return 'Modus Tolens cannot be performed on these two premises.';
        }

      // DD: Direct Derivation: Check if the premise matches the conclusion.
      case 'DD':
        if (this.premise.equalsPremise(this.conclusion)) {
          return this.premise;
        } else {
          return 'That is not the correct premise for a Direct Derivation.';
        }

      // CD: Conditional Derivation: Check if the premise matches the consequent
      case 'CD':
        if (this.premise.equalsPremise(this.consequent)) {
          return this.conclusion;
        } else {
          return 'That is not the correct premise for a Conditional ' +
            'Derivation.';
        }

      // ID: Indirect Derivation: Check to see if the two premises are exact
      // logical opposites. If so, this implies a contradiction which solves the
      // show.
      case 'ID':
        if (this.premise1.type === 'not' &&
            this.premise2.equalsPremise(this.premise1.negated)) {
          return this.conclusion;
        } else if (this.premise2.type === 'not' &&
            this.premise1.equalsPremise(this.premise2.negated)) {
          return this.conclusion;
        } else {
          return 'Cannot perform an Indirect Derivation with the provided ' +
            'Premises';
        }

      // DN: Double Negation: Check to see if the premise is a not within a
      // not, and return the inner premise, otherwise put a double negation
      // on the premise.
      case 'DN':
        if (this.premise.type === 'not' &&
            this.premise.premise.type === 'not') {
          return this.premise.premise.premise;
        } else {
          const newInnerPremise = new Premise({
            type: 'not', premise1: this.premise});
          return new Premise({type: 'not', premise1: newInnerPremise});
        }

      // DNE: Double Negation Elimination: Check to see if the premise is
      // double negated and returnt he inner premise if so.
      case 'DNE':
        if (this.premise.type === 'not' &&
            this.premise.premise.type === 'not') {
          return this.premise.premise.premise;
        } else {
          return 'Cannot invoke DNE on a premise that is not double negated.';
        }
      // DNI: Double Negation Introduction: Put a double negation on the
      // premise.
      case 'DNI':
        const newInnerPremise = new Premise({
          type: 'not', premise1: this.premise});
        return new Premise({type: 'not', premise1: newInnerPremise});

      // Simplification Rules.
      case 'SR':
        if (this.premise.type === 'and') {
          return this.premise.premise2;
        }
        return 'Cannot invoke SR on a premise that is not an conjunction.';
      case 'S':
        return this.premise2;
      case 'SL':
        if (this.premise.type === 'and') {
          return this.premise.premise1;
        }
        return `Cannot invoke ${this.name} on a premise that is not a ` +
        'conjunction.';

      // Adjunction
      case 'Adj':
        return new Premise({
          type: 'and',
          premise1: this.premise1,
          premise2: this.premise2,
        });

      // Addition
      case 'Add':
      case 'AddL':
        return new Premise({
          type: 'or',
          premise1: this.premise1,
          premise2: this.premise2,
        });
      case 'AddR':
        return new Premise({
          type: 'or',
          premise1: this.premise2,
          premise2: this.premise1,
        });

      // Modus Tollendo Ponens
      case 'MTP':
        if (this.premise1.type === 'not' && this.premise2.type === 'or') {
          if (this.premise1.premise.equalsPremise(this.premise2.premise1)) {
            return this.premise2.premise2;
          } else if (this.premise1.premise.equalsPremise(
              this.premise2.premise2)) {
            return this.premise2.premise1;
          } else {
            return 'Negated premise must match one of the premises in the ' +
                'disjunction.';
          }
        } else if (this.premise2.type === 'not' &&
            this.premise1.type === 'or') {
          if (this.premise2.premise.equalsPremise(this.premise1.premise1)) {
            return this.premise1.premise2;
          } else if (this.premise2.premise.equalsPremise(
              this.premise1.premise2)) {
            return this.premise1.premise1;
          } else {
            return 'Negated premise must match one of the premises in the ' +
                'disjunction.';
          }
        } else {
          return 'MTP can only be invoked on a negation and a disjunction.';
        }

      // Biconditional-conditional
      case 'BC':
        return this.premise2;
      case 'BCL':
        if (this.premise.type === 'biconditional') {
          return this.premise.premise2;
        } else {
          return `${this.name} can only be invoked on a biconditional premise.`;
        }
      case 'BCR':
        if (this.premise.type === 'biconditional') {
          return this.premise.premise1;
        } else {
          return 'BCR can only be invoked on a biconditional premise.';
        }

      // Conditional-biconditional
      case 'CB':
        if (this.premise1.type === 'conditional' &&
          this.premise2.type === 'conditional') {
          if (this.premise1.antecedent.equalsPremise(this.premise2.consequent)
            &&
            this.premise2.antecedent.equalsPremise(this.premise1.consequent)) {
            return new Premise({
              type: 'biconditional',
              premise1: this.premise1,
              premise2: this.premise2,
            });
          } else {
            return 'Conditionals must be exact opposites of each other in ' +
              'order to invoke CB on them.';
          }
        } else {
          return 'CB can only be invoked on two conditional premises.';
        }

      // Material Conditional
      case 'MC1':
        return new Premise({
          type: 'conditional',
          premise1: this.premise2,
          premise2: this.premise1,
        });
      case 'MC2':
        if (this.premise1.type === 'not') {
          return new Premise({
            type: 'conditional',
            premise1: this.premise1.premise,
            premise2: this.premise2,
          });
        } else {
          return 'MC2 can only be invoked on a negated premise.';
        }
      case 'MC':
        if (this.premise1.type === 'not') {
          return new Premise({
            type: 'conditional',
            premise1: this.premise1.premise,
            premise2: this.premise2,
          });
        } else {
          return new Premise({
            type: 'conditional',
            premise1: this.premise2,
            premise2: this.premise1,
          });
        }
    }
  }

  // ---------------------------Getters-----------------------------------------

  /**
   * Getter for the number of Premises that this Rule will take.
   * @return {int} - The number of allowed Premises for this Rule.
   */
  get allowedPremises() {
    return this._allowedPremises;
  }

  /**
   * Getter for the name of the current Rule.
   * @return {string} - Name of the current Rule.
   */
  get name() {
    return this._name;
  }

  // ---------------------------Setters-----------------------------------------

  /**
   * Setter for the name property.
   * @param {string} value - representing the name of the Rule.
   */
  set name(value) {
    this._name = value;
  }

  /**
   * Setter for the allowedPremises property.
   * @param {int} value - representing number of allowed Premises for the Rule.
   */
  set allowedPremises(value) {
    this._allowedPremises = value;
  }
}

export default Rule;
