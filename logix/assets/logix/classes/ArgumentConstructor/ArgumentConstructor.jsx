import PremiseConstructor from '../PremiseConstructor';
import Argument from '../Argument';

/**
 * ArgumentConstructor class to hold methods that construct the argument
 * string and argument itself from the argument JSON that comes from backend.
 */
class ArgumentConstructor {
  /**
   * constructor for ArgumentConstructor class.
   */
  constructor() {
    this.premiseConstructor = new PremiseConstructor();
  }

  /**
   * Function to construct and return an Argument from a JSON.
   * @param {JSON} argJSON - JSON of argument from backend.
   * @return {Argument} - new Argument object.
   */
  getArgumentFromJSON(argJSON) {
    const premises = [];
    argJSON.premises.forEach(function(premiseJSON) {
      premises.push(this.premiseConstructor.getPremiseFromJSON(premiseJSON));
    }.bind(this));
    const conclusion = this.premiseConstructor.getPremiseFromJSON(
        argJSON.conclusion);
    return new Argument(premises, conclusion);
  }

  /**
   * Fucntion to take argument JSON from backend and turn it into a string.
   * @param {JSON} argJSON - incoming argument JSON.
   * @return {string} - string of argument.
   */
  stringFromJSON(argJSON) {
    let retString = '';
    argJSON.argument.premises.forEach(function(premise) {
      retString += `${this.premiseConstructor.stringFromJSON(premise)}. `;
    }.bind(this));
    retString += '∴ ';
    retString += `${this.premiseConstructor.stringFromJSON(
        argJSON.argument.conclusion)}.`;
    return retString;
  }
}

export default ArgumentConstructor;
