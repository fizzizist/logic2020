// eslint-disable-next-line no-unused-vars
import React from 'react';
import ReactDOM from 'react-dom';
// eslint-disable-next-line no-unused-vars
import ArgumentConstructorApp from './components/app';

ReactDOM.render(
    <div>
      <link
        rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity={'sha384-g  gOyR0iXCbMQv3Xipma34MD+dH/1fQ78' +
        '4/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T'}
        crossOrigin="anonymous"
      />
      <ArgumentConstructorApp isAdmin/>
    </div>,
    document.getElementById('react'),
);
