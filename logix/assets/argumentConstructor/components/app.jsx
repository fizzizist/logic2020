import React, {Component} from 'react';
import PremiseConstructor from '../../logix/classes/PremiseConstructor';
import Argument from '../../logix/classes/Argument';
// eslint-disable-next-line no-unused-vars
import {ButtonToolbar, Button, Form, Alert, Card} from 'react-bootstrap';
import axios from 'axios';

/**
 * Main App component for ArgumentConstructor App inside of logix
 * This App is meant to be mostly for admin users to add arguments to the
 * database through a PremiseConstructor-like interface.
 */
class ArgumentConstructorApp extends Component {
  /**
   * constructor for ArgumentConstructor component.
   * @param {array} props - props from parent component.
   */
  constructor(props) {
    super(props);
    this.updatePremiseString = this.updatePremiseString.bind(this);
    const premiseConstructor = new PremiseConstructor(this.updatePremiseString);
    let callback;
    if (this.props.updateCallback) {
      callback = this.props.updateCallback;
    } else {
      callback = () => {};
    }
    this.state = {
      premiseConstructor: premiseConstructor,
      premises: [],
      conclusion: null,
      argument: '',
      premiseString: '',
      premiseNum: 1,
      errorMessage: '',
      argumentReady: false,
      successMessage: false,
      argSet: 0,
      callback,
    };
    this.inputArgSet = React.createRef();
    this.submitPremiseString = this.submitPremiseString.bind(this);
    this.submitConclusion = this.submitConclusion.bind(this);
    this.submitArgument = this.submitArgument.bind(this);
    this.changeArgSet = this.changeArgSet.bind(this);
    this.clearPremise = this.clearPremise.bind(this);
    this.restartArgument = this.restartArgument.bind(this);
  }

  /**
   * Callback function passed into PremiseConstructor to update the premise
   * string shown to the user.
   */
  updatePremiseString() {
    this.setState({
      premiseString: this.state.premiseConstructor.premiseString,
      successMessage: false,
      errorMessage: '',
    });
  }

  /**
   * on-click function for premise submit button that adds the premise to the
   * argument.
   */
  submitPremiseString() {
    const result = this.state.premiseConstructor.resultingPremise;
    if (typeof result === 'string') {
      this.setState({
        errorMessage: results,
        premiseString: '',
      });
      this.state.premiseConstructor.premiseString = '';
    } else {
      this.setState((state) => ({
        premises: state.premises.concat(result),
        argument: this.state.argument +
          `PR${state.premiseNum}: ${state.premiseString} `,
        premiseString: '',
        premiseNum: state.premiseNum + 1,
      }));
    }
  }

  /**
   * on-click function for the Submit Conclusion button.
   */
  submitConclusion() {
    const result = this.state.premiseConstructor.resultingPremise;
    if (typeof result === 'string') {
      this.setState({
        errorMessage: results,
        premiseString: '',
      });
      this.state.premiseConstructor.premiseString = '';
    } else {
      this.setState((state) => ({
        argument: state.argument + `C: ${state.premiseString}`,
        premiseString: '',
        argumentReady: true,
        conclusion: result,
      }));
    }
  }

  /**
   * on-click function for the Submit Argument button.
   */
  submitArgument() {
    const argument = new Argument(this.state.premises, this.state.conclusion);
    const inputSetValue = this.state.argSet;
    axios({
      method: 'POST',
      url: '/api/arguments/',
      headers: {
        'X-CSRFToken': CSRF_TOKEN,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      data: {
        argument: argument.toJSON(),
        argument_set: inputSetValue,
      },
    })
        .then(function(response) {
          this.setState({
            successMessage: true,
            argumentReady: false,
            argument: '',
            premiseNum: 1,
            premises: [],
          }, this.state.callback);
        }.bind(this))
        .catch(function(error) {
          this.setState({
            errorMessage: error.response.statusText,
          });
        }.bind(this));
  }

  /**
   * Function that runs when the argument set is changed.
   */
  changeArgSet() {
    this.setState({
      argSet: this.inputArgSet.current.value,
    });
  }

  /**
   * on-click function to clear the premise string.
   */
  clearPremise() {
    this.state.premiseConstructor.premiseString = '';
    this.setState({
      premiseString: '',
    });
  }

  /**
   * on-click function for restarting the argument.
   */
  restartArgument() {
    this.state.premiseConstructor.premiseString = '';
    this.setState({
      argumentReady: false,
      argument: '',
      premiseNum: 1,
      premises: [],
      premiseString: '',
      errorMessage: '',
    });
  }

  /**
   * Render for the main App component of the ArgumentConstructor App.
   * @return {HTML} - Return HTML output of the component.
   */
  render() {
    return (
      <div>
        <Card>
          <Card.Body>
            <Card.Title>Argument</Card.Title>
            <Card.Text>{this.state.argument}</Card.Text>
          </Card.Body>
        </Card>
        <Card>
          <Card.Body>
            <Card.Title>Premise</Card.Title>
            <Card.Text>{this.state.premiseString}</Card.Text>
          </Card.Body>
        </Card>
        {this.state.argumentReady &&
            <ButtonToolbar>
              <Button
                variant={'dark'}
                onClick={this.submitArgument}>Submit Argument</Button>
              <Button
                variant={'dark'}
                onClick={this.restartArgument}>Restart</Button>
            </ButtonToolbar>
        }
        {!this.state.argumentReady &&
          <React.Fragment>
            {this.props.isAdmin &&
            <Form.Group>
              <Form.Label>Argument Set</Form.Label>
              <Form.Control
                value={this.state.argSet}
                as="select"
                ref={this.inputArgSet}
                onChange={this.changeArgSet}
              >
                <option value={0}>Custom</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
              </Form.Control>
            </Form.Group>
            }
            <ButtonToolbar>
              {this.state.premiseConstructor.buttons}
            </ButtonToolbar>
            <ButtonToolbar>
              <Button
                variant={'dark'}
                onClick={this.submitPremiseString}>Submit Premise</Button>
              <Button
                variant={'dark'}
                onClick={this.submitConclusion}>Submit Conclusion</Button>
              <Button
                variant={'dark'}
                onClick={this.clearPremise}>Clear Premise</Button>
              <Button
                variant={'dark'}
                onClick={this.restartArgument}>Restart</Button>
            </ButtonToolbar>
          </React.Fragment>
        }
        {this.state.errorMessage &&
        <Alert variant={'danger'}>{
          this.state.errorMessage
        }. Argument must be unique, and in the correct format.</Alert>
        }
        {this.state.successMessage &&
          <Alert
            variant={'success'}
          >The argument was submitted to the database successfully.</Alert>
        }
      </div>
    );
  }
}

export default ArgumentConstructorApp;
