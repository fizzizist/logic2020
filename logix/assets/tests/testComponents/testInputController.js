import React from 'react'; // eslint-disable-line no-unused-vars
import {assert} from 'chai';
import {shallow, mount} from 'enzyme';
import axios from 'axios';
import sinon from 'sinon';
// eslint-disable-next-line no-unused-vars
import InputController from '../../logix/components/InputController';
import Premise from '../../logix/classes/Premise';
import Rule from '../../logix/classes/Rule';

describe('InputController unit tests', () => {
  let sandbox;
  beforeEach(() => sandbox = sinon.createSandbox());
  afterEach(() => sandbox.restore());
  const ruleMP = new Rule('MP');
  const prem1 = new Premise({type: 'atomic', premise1: 'P', id: 'PR1'});
  const prem2 = new Premise({type: 'atomic', premise1: 'Q', id: '2'});
  const prem3 = new Premise({type: 'atomic', premise1: 'R'});
  const prem4 = new Premise({
    type: 'conditional',
    premise1: prem1,
    premise2: prem2,
    id: 'PR2',
  });
  const conclusionPrem = new Premise({
    type: 'conditional',
    premise1: prem2,
    premise2: prem3,
  });
  const andPrem = new Premise({
    type: 'and',
    premise1: prem1,
    premise2: prem2,
  });

  it('tests initial state', () => {
    const resolved = new Promise((r) => r({data: [{label: 'MP'}]}));
    sandbox.stub(axios, 'get').returns(resolved);
    const wrapper = new Promise((resolve) => {
      resolve(shallow(<InputController premises={[prem1]}
        conclusion={conclusionPrem} innerPremises={[]} outerPremises={[]}/>));
    });
    wrapper.then((value) => {
      assert(value.state('availableRules').length === 2,
          '2 initial available rules before backend request.');
      assert(value.state('premises').length === 1, '1 premises from props');
      assert(value.state('availablePremises').length === 1,
          '1 available premise');
    });
  });

  it('tests assumeCD', () => {
    const resolved = new Promise((r) => r({data: []}));
    sandbox.stub(axios, 'get').resolves(resolved);
    const wrapper = shallow(<InputController premises={[prem1]}
      conclusion={conclusionPrem} innerPremises={[]} outerPremises={[]}/>);
    wrapper.instance().assumeCD();
    assert(wrapper.state('inputString') === 'Ass CD',
        'Ass CD should appear in command box');
    assert(wrapper.state('selectedPremises').length === 1,
        'antecedent should be selected');
    assert(wrapper.state('assumeSelected') === true,
        'assumeSelected should be true');
    assert(wrapper.state('availableRules').length === 1,
        'CD should be added to availableRules');
  });

  it('tests assumeID', () => {
    const resolved = new Promise((r) => r({data: []}));
    sandbox.stub(axios, 'get').resolves(resolved);
    const wrapper = shallow(<InputController premises={[prem1]}
      conclusion={conclusionPrem} innerPremises={[]} outerPremises={[]}/>);
    wrapper.instance().assumeID();
    assert(wrapper.state('inputString') === 'Ass ID',
        'Ass CD should appear in command box');
    assert(wrapper.state('selectedPremises').length === 1,
        'antecedent should be selected');
    assert(wrapper.state('assumeSelected') === true,
        'assumeSelected should be true');
    assert(wrapper.state('availableRules').length === 1,
        'CD should be added to availableRules');
  });

  it('tests resetButtons', () => {
    const resolved = new Promise((r) => r({data: []}));
    sandbox.stub(axios, 'get').resolves(resolved);
    const wrapper = shallow(<InputController premises={[prem1]}
      conclusion={conclusionPrem} innerPremises={[]} outerPremises={[]}/>);
    wrapper.instance().assumeID();
    wrapper.instance().resetButtons();
    assert(wrapper.state('inputString') === '',
        'Ass CD should appear in command box');
    assert(wrapper.state('selectedPremises').length === 0,
        'antecedent should be selected');
    assert(wrapper.state('assumeSelected') === false,
        'assumeSelected should be true');
    assert(wrapper.state('availableRules').length === 1,
        'DD should be added to availableRules');
  });

  it('tests selectPremise', () => {
    const resolved = new Promise((r) => r({data: []}));
    sandbox.stub(axios, 'get').resolves(resolved);
    const wrapper = shallow(<InputController premises={[prem1]}
      conclusion={conclusionPrem} innerPremises={[]} outerPremises={[]}/>);
    wrapper.instance().selectPremise(prem1);
    assert(wrapper.state('selectedPremises').length === 1,
        'a premise should be added to selectedPremises');
  });

  it('tests selectRule MP', () => {
    const resolved = new Promise((r) => r({data: []}));
    sandbox.stub(axios, 'get').resolves(resolved);
    const wrapper = shallow(<InputController premises={[prem1, prem4]}
      conclusion={conclusionPrem} innerPremises={[]} outerPremises={[]}/>);

    wrapper.instance().selectPremise(prem1);
    wrapper.instance().selectPremise(prem4);
    wrapper.instance().selectRule(ruleMP);

    assert(wrapper.state('selectedPremises').length === 1,
        'a premise should be added to selectedPremises');
    assert(wrapper.state('selectedRules').length === 1,
        'a new rule should be added to selectedRules');
    assert(wrapper.state('inputString') === ' PR1 PR2 MP',
        'PR1 PR2 MP should appear in command box');
    assert(wrapper.state('submitToggle') === true,
        'submit button should be available');
  });

  it('tests selectRule (all rules)', () => {
    const resolved = new Promise((r) => r({
      data: [
        {label: 'MP'},
        {label: 'MT'},
        {label: 'DN'},
        {label: 'DNE'},
        {label: 'DNI'},
        {label: 'S'},
        {label: 'SR'},
        {label: 'SL'},
        {label: 'Adj'},
        {label: 'Add'},
        {label: 'AddR'},
        {label: 'AddL'},
        {label: 'MTP'},
        {label: 'BCL'},
        {label: 'BCR'},
        {label: 'CB'},
        {label: 'MC1'},
        {label: 'MC2'},
        {label: 'MC'},
        {label: 'ID'},
        {label: 'CD'},
      ]}));
    sandbox.stub(axios, 'get').returns(resolved);
    let confirmCalled = 0;
    let toggleCalled = 0;
    /**
     * Mock function for confirmNewRule
     * @param {Rule} rule - rule passed from testing function.
     */
    function confirmNewRule(rule) {
      confirmCalled++;
    }
    /**
     * Mock function for toggleRuleName
     * @param {string} ruleName - rule name passed from testing function.
     */
    function toggleRuleMenu(ruleName) {
      toggleCalled++;
    }
    const wrapper = new Promise((resolve) => {
      resolve(shallow(<InputController premises={[prem1]}
        conclusion={conclusionPrem} innerPremises={[]} outerPremises={[]}/>));
    });

    wrapper.then((value) => {
      value.instance().confirmNewRule = confirmNewRule;
      value.instance().toggleRuleMenu = toggleRuleMenu;
      value.instance().setState({
        selectedPremises: [prem1, prem2],
      });
      value.state('availableRules').forEach((rule, _) => {
        value.instance().selectRule(rule);
      });
      assert(confirmCalled === 15, 'confirmNewRule should get called 16 times');
      assert(toggleCalled === 6, 'toggleRuleMenu should get called 6 times.');
    }).catch((error) => {
      console.log(error);
    });
  });

  it('tests selectRule MP error', () => {
    const resolved = new Promise((r) => r({data: []}));
    sandbox.stub(axios, 'get').resolves(resolved);
    const wrapper = shallow(<InputController premises={[prem1, prem2]}
      conclusion={conclusionPrem} innerPremises={[]} outerPremises={[]}/>);
    wrapper.instance().selectPremise(prem1);
    wrapper.instance().selectPremise(prem2);
    wrapper.instance().selectRule(ruleMP);
    assert(wrapper.state('selectedPremises').length === 0,
        'selectedPremises should be reset to zero');
    assert(wrapper.state('selectedRules').length === 0,
        'selectedRules should be reset');
    assert(wrapper.state('inputString') === '',
        'inputString should be reset');
    assert(wrapper.state('submitToggle') === false,
        'submit button should be unavailable');
    assert(wrapper.state('assumeSelected') === false,
        'assumeSelected should be false');
    assert(wrapper.state('errorMessage') ===
    'Modus Ponens cannot be performed on these premises.',
    'an error message should occur.');
  });

  it('tests submitCommand assume CD', () => {
    const resolved = new Promise((r) => r({data: []}));
    sandbox.stub(axios, 'get').resolves(resolved);
    /**
     * empty callback function.
     * @param {Premise} premise
     */
    function submitCallback(premise) {
      // pass
    }
    const wrapper = shallow(<InputController premises={[prem1, prem4]}
      conclusion={conclusionPrem} innerPremises={[]} outerPremises={[]}
      lineNumber={1} submitCommand={submitCallback}/>);
    wrapper.instance().assumeCD();
    wrapper.instance().submitCommand();
    assert(wrapper.state('selectedPremises').length === 0,
        'selected premises should be reset to zero');
    assert(wrapper.state('inputString') === '',
        'inputString should be reset');
    assert(wrapper.state('assumeSubmitted') === true,
        'assumedSubmitted should be true now.');
  });

  it('tests submitCommand assumeSubmitted already true', () => {
    const premP = new Premise({type: 'atomic', premise1: 'P'});
    const resolved = new Promise((r) => r({data: []}));
    sandbox.stub(axios, 'get').resolves(resolved);
    /**
     * empty callback function.
     * @param {Premise} premise
     */
    function submitCallback(premise) {
      // pass
    }
    const wrapper = shallow(<InputController premises={[prem1, prem4]}
      conclusion={conclusionPrem} innerPremises={[]} outerPremises={[]}
      lineNumber={1} submitCommand={submitCallback}/>);
    wrapper.instance().setState({
      inputString: 'test',
      selectedPremises: [premP],
      assumeSubmitted: true,
    });
    wrapper.instance().submitCommand();
    assert(wrapper.state('selectedPremises').length === 0,
        'selected premises should be reset to zero');
    assert(wrapper.state('inputString') === '',
        'inputString should be reset');
    assert(wrapper.state('assumeSubmitted') === true,
        'assumedSubmitted should remain true.');
    assert(premP.id === '1', 'premise ID should be set to line number.');
    assert(premP.commandText === 'test',
        'commandText should be set to inputString');
  });

  it('tests confirmRuleModal', () => {
    const resolved = new Promise((r) => r({data: []}));
    sandbox.stub(axios, 'get').resolves(resolved);
    const wrapper = shallow(<InputController premises={[prem1]}
      conclusion={conclusionPrem} innerPremises={[]} outerPremises={[]}/>);
    let newRule;
    /**
     * Mock function for confirmNewRule
     * @param {Rule} rule - rule passed from testing function.
     */
    function confirmNewRule(rule) {
      newRule = rule;
    }
    wrapper.instance().confirmNewRule = confirmNewRule;
    wrapper.state('premiseConstructor').premiseString = 'P';
    wrapper.instance().setState({
      selectedRule: 'Add',
      selectedPremises: [prem3],
    });
    wrapper.instance().confirmRuleModal();
    assert(newRule.name === 'Add', 'new Rule should be Add');
    const newPrem = newRule.resultingPremise;
    assert(newPrem.premiseString === 'R v P',
        'Resulting premise should be the result of the Add rule.');
  });

  it('tests toggleRuleMenu', () => {
    const resolved = new Promise((r) => r({data: []}));
    sandbox.stub(axios, 'get').resolves(resolved);
    const wrapper = shallow(<InputController premises={[prem1]}
      conclusion={conclusionPrem} innerPremises={[]} outerPremises={[]}/>);

    wrapper.instance().toggleRuleMenu();
    assert(wrapper.state('showRuleModal'), 'showRuleModal should be true');
    assert(wrapper.state('selectedRule') === '',
        'selectedRule should default to an empty string');

    wrapper.setState({
      showMenuString: 'P',
    });
    wrapper.state('premiseConstructor').premiseString = 'P';

    wrapper.instance().toggleRuleMenu();
    assert(!wrapper.state('showRuleModal'), 'showRuleModal should be false');
    assert(wrapper.state('showMenuString') === '',
        'showMenuString should be reset to an empty string');
    assert(wrapper.state('premiseConstructor').premiseString === '',
        'premiseConstructor string should be set to empty');
  });

  it('tests toggleShowMenu', () => {
    const resolved = new Promise((r) => r({data: []}));
    sandbox.stub(axios, 'get').resolves(resolved);
    const wrapper = shallow(<InputController premises={[prem1]}
      conclusion={conclusionPrem} innerPremises={[]} outerPremises={[]}/>);

    wrapper.instance().toggleShowMenu();
    assert(wrapper.state('showShowMenuModal'), 'showRuleModal should be true');

    wrapper.setState({
      showMenuString: 'P',
    });
    wrapper.state('premiseConstructor').premiseString = 'P';

    wrapper.instance().toggleShowMenu();
    assert(!wrapper.state('showShowMenuModal'),
        'showRuleModal should be false');
    assert(wrapper.state('showMenuString') === '',
        'showMenuString should be reset to an empty string');
    assert(wrapper.state('premiseConstructor').premiseString === '',
        'premiseConstructor string should be set to empty');
  });

  it('tests updateShowModal', () => {
    const resolved = new Promise((r) => r({data: []}));
    sandbox.stub(axios, 'get').resolves(resolved);
    const wrapper = shallow(<InputController premises={[prem1]}
      conclusion={conclusionPrem} innerPremises={[]} outerPremises={[]}/>);
    assert(wrapper.state('showMenuString') === '',
        'showMenuString is empty by default.');
    wrapper.state('premiseConstructor').premiseString = 'P';
    wrapper.instance().updateShowModal();
    assert(wrapper.state('showMenuString') === 'P',
        'showMenuString should get updated to premiseConstructor value.');
  });

  it('tests confirmRuleResultPrompt nothing checked', function() {
    const resolved = new Promise((r) => r({data: []}));
    sandbox.stub(axios, 'get').resolves(resolved);
    const wrapper = mount(<InputController premises={[prem1]}
      conclusion={conclusionPrem} innerPremises={[]} outerPremises={[]}/>,
    );
    wrapper.instance().setState({
      selectedPremises: [andPrem],
      showRuleResultPrompt: true,
    }, wrapper.instance().confirmRuleResultPrompt);
    assert(wrapper.state('errorMessage') ===
        'You must choose an output for the rule.');
    assert(!wrapper.state('showRuleResultPrompt'));
  });
});
