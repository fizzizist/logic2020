import React from 'react'; // eslint-disable-line no-unused-vars
// eslint-disable-next-line no-unused-vars
import Derivation from '../../logix/components/Derivation';
import Premise from '../../logix/classes/Premise';
import {mount, shallow} from 'enzyme';
import {assert} from 'chai';


describe('Derivation unit tests', () => {
  const argumentData = {
    premises: [{type: 'atomic', symbol: 'P'}],
    conclusion: {type: 'atomic', symbol: 'P'},
  };
  const prevSolution = [[{lineNumber: 1, output: 'P', command: ''}]];

  it('tests showC', () => {
    const argumentID = 1;
    const wrapper = shallow(<Derivation argument={argumentData}
      argumentID={argumentID}
      solutions={[]}/>);
    wrapper.instance().showC();
    assert(wrapper.state('showing') === true,
        'toggle showing state');
  });

  it('tests initial state with solutions > 0', () => {
    const wrapper = shallow(
        <Derivation
          argument={argumentData}
          argumentID={1}
          solutions={prevSolution}
        />);
    assert(wrapper.state('solved') === true,
        'solved should be true by default');
    assert(wrapper.state('showing') === true,
        'showing needs to be true by default');
    assert(wrapper.state('selectSolutions') === true,
        'selectSolutions should be true');
    assert(wrapper.state('solutionsCount') === 1,
        'solutionsCount should be the number of solutions that exist.');
  });

  it('tests initial state with joyRide and solutions', () => {
    const wrapper = shallow(
        <Derivation
          argument={argumentData}
          argumentID={1}
          solutions={prevSolution}
          joyrideRunning
        />);
    assert(wrapper.state('solved') === false,
        'solved should be true by default');
    assert(wrapper.state('showing') === false,
        'showing needs to be true by default');
    assert(wrapper.state('selectSolutions') === false,
        'selectSolutions should be true');
    assert(wrapper.state('solutionsCount') === 2,
        'solutionsCount should be the number of solutions that exist.');
  });

  it('tests solvedCallback function', () => {
    const premise = new Premise({
      type: 'atomic',
      premise1: 'P',
    });
    const lineInfo = [{lineNumber: 1, output: 'Q', command: ''}];
    const wrapper = mount(
        <Derivation
          argument={argumentData}
          argumentID={1}
          solutions={[]}
        />);
    wrapper.instance().putArgumentSolution = () => {};
    wrapper.update();
    wrapper.instance().solvedCallback(premise, lineInfo, 1);
    assert(wrapper.state('solutions').length === 1,
        'lineInfo should be added to solutions array.');
  });
  it('tests changeCurrentSolution', () => {
    const wrapper = shallow(
        <Derivation
          argument={argumentData}
          argumentID={1}
          solutions={prevSolution}
        />);
    wrapper.instance().inputCurrentSolution = {current: {value: 3}};
    wrapper.update();
    wrapper.instance().changeCurrentSolution();
    assert(wrapper.state('currentSolution') === 3,
        'currentSolution should get set to 3');
  });
  it('tests restartDerivation solved', () => {
    const wrapper = shallow(
        <Derivation
          argument={argumentData}
          argumentID={1}
          solutions={prevSolution}
        />);
    wrapper.instance().restartDerivation();
    assert(wrapper.state('solutionsCount') === 2,
        'solutionsCount should be incremented by 1');
    assert(wrapper.state('currentSolution') === 2,
        'currentSolution should be set to solutionsCount');
    assert(wrapper.state('selectSolutions') === false,
        'selection of solutions should be unavailable');
    assert(wrapper.state('solved') === false,
        'solved should be set to false');
    assert(wrapper.state('showing') === false,
        'showing is set to false by default');
  });
  it('tests restartDerivation not solved', () => {
    const wrapper = shallow(
        <Derivation
          argument={argumentData}
          argumentID={1}
          solutions={[]}
        />);
    wrapper.instance().restartDerivation();
    assert(wrapper.state('restarting') === true,
        'restarting gets set to true');
  });
});
