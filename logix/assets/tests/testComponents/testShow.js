import React from 'react'; // eslint-disable-line no-unused-vars
// eslint-disable-next-line no-unused-vars
import Show from '../../logix/components/Show';
import Premise from '../../logix/classes/Premise';
import {shallow} from 'enzyme';
import {assert, expect} from 'chai';

describe('Show unit tests', () => {
  const prem1 = new Premise({type: 'atomic', premise1: 'P', id: 'PR1'});
  const prem2 = new Premise({type: 'atomic', premise1: 'Q', id: '2'});
  const prem3 = new Premise({type: 'atomic', premise1: 'R'});
  const conclusionPrem = new Premise({
    type: 'conditional',
    premise1: prem2,
    premise2: prem3});
  it('tests initial Show state', () => {
    const wrapper = shallow(<Show premises={[prem1, prem2]}
      conclusion={conclusionPrem} lastNumber={1}
      outerPremises={[prem3]}
      lineInfo={[]}/>);
    assert(wrapper.state('lineNumber') === 2,
        'lineNumber should be 1 plus lastNumber');
    assert(wrapper.state('ownLineNumber') === 1,
        'ownLineNumber should be lastNumber');
    assert(wrapper.state('lineInfo').length === 1,
        'lines should only have the Show line in it.');
    assert(wrapper.state('outerPremises').length === 1,
        'should populate state with outerPremises');
  });
  it('tests initial state with lineInfo', () => {
    const wrapper = shallow(
        <Show
          premises={[prem1, prem2]}
          conclusion={conclusionPrem} lastNumber={1}
          outerPremises={[prem3]}
          lineInfo={[{lineNumber: 1, output: 'P', command: ''}]}
        />);
    expect(wrapper.state('lineInfo')).to.eql(
        [{lineNumber: 1, output: 'P', command: ''}]);
    expect(wrapper.state('solved')).to.equal(true);
  });
  it('tests submitCommandCallback (solved false)', () => {
    const wrapper = shallow(<Show premises={[prem1, prem2]}
      conclusion={conclusionPrem} lastNumber={1}
      outerPremises={[prem3]}
      lineInfo={[]}
    />);
    wrapper.instance().submitCommandCallback(prem3);
    assert(wrapper.state('lineInfo').length === 2,
        'adds a show line');
    assert(wrapper.state('linePremises').length === 1,
        'adds a new line premise.');
    assert(wrapper.state('lineNumber') === 3,
        'increments line number by 1');
  });
  it('tests toggleCollapsed to false', () => {
    const wrapper = shallow(<Show premises={[prem1, prem2]}
      conclusion={conclusionPrem} lastNumber={1}
      outerPremises={[prem3]}
      lineInfo={[]}
    />);
    wrapper.instance().toggleCollapsed(1);
    assert(wrapper.state('collapsed')[1] === false,
        'collapsed for index 1 should flip to false');
  });
  it('tests toggleCollapsed to true', () => {
    const wrapper = shallow(
        <Show
          premises={[prem1, prem2]}
          conclusion={conclusionPrem}
          lineInfo={[]}
          lastNumber={1}
          outerPremises={[prem3]}/>);
    wrapper.instance().setState({
      collapsed: {1: false},
    });
    wrapper.instance().toggleCollapsed(1);
    assert(wrapper.state('collapsed')[1] === true,
        'collapsed for index 1 should flip to true');
  });
  it('tests restartShow', function() {
    const wrapper = shallow(
        <Show
          premises={[prem1, prem2]}
          conclusion={conclusionPrem}
          lastNumber={1}
          outerPremises={[prem3]}
          restarting={false}
          restartCallback={() => null}
          lineInfo={[]}
        />);
    // set state to things that are not the default.
    wrapper.instance().setState({
      lineInfo: [{lineNumber: 3, output: 'test', command: ''}],
      outerPremises: [],
      linePremises: [prem3],
      lineNumber: 11,
      ownLineNumber: 12,
      collapse: {
        2: false,
      },
    });
    // reset show to default settings
    wrapper.setProps({
      restarting: true,
    });
    // assert default settings
    assert(wrapper.state('lineInfo')[0].lineNumber === 1,
        'displayDict should be returned to default');
    assert(wrapper.state('outerPremises')[0].equalsPremise(prem3),
        'outerPremises should be reset to default');
    assert(wrapper.state('linePremises').length === 0,
        'linePremises should be an empty array');
    assert(wrapper.state('lineNumber') === 2,
        'lineNumber should be reset to 2');
    assert(wrapper.state('ownLineNumber') === 1,
        'ownLineNumber should never really change but we reset it anyway');
    expect(wrapper.state('collapsed')).to.eql({'1': true});
  });
});
