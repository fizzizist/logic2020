import {assert} from 'chai';
import PremiseConstructor from '../../logix/classes/PremiseConstructor';
import Premise from '../../logix/classes/Premise';

describe('PremiseConstructor tests', function() {
  it('makeCustomPremise basic conditional', function() {
    const pc = new PremiseConstructor();
    const premise = pc.makeCustomPremise('P→Q');
    assert.equal(premise.premiseString, 'P → Q');
    assert.equal(premise.type, 'conditional');
  });

  it('makeCustomPremise basic biconditional', function() {
    const pc = new PremiseConstructor();
    const premise = pc.makeCustomPremise('P↔Q');
    assert.equal(premise.premiseString, 'P ↔ Q');
    assert.equal(premise.type, 'biconditional');
  });

  it('makeCustomPremise basic or', function() {
    const pc = new PremiseConstructor();
    const premise = pc.makeCustomPremise('PvQ');
    assert.equal(premise.premiseString, 'P v Q');
    assert.equal(premise.type, 'or');
  });

  it('makeCustomPremise basic and', function() {
    const pc = new PremiseConstructor();
    const premise = pc.makeCustomPremise('P^Q');
    assert.equal(premise.premiseString, 'P ^ Q');
    assert.equal(premise.type, 'and');
  });

  it('makeCustomPremise basic not', function() {
    const pc = new PremiseConstructor();
    const premise = pc.makeCustomPremise('~P');
    assert.equal(premise.premiseString, '~P');
    assert.equal(premise.type, 'not');
  });

  it('makeCustomPremise conditional bracketed', function() {
    const pc = new PremiseConstructor();
    const premise = pc.makeCustomPremise('(P→Q)→R');
    assert.equal(premise.premiseString, '(P → Q) → R');
    assert.equal(premise.type, 'conditional');
  });

  it('makeCustomPremise conditional negated antecedent', function() {
    const pc = new PremiseConstructor();
    const premise = pc.makeCustomPremise('~P→Q');
    assert.equal(premise.premiseString, '~P → Q');
    assert.equal(premise.type, 'conditional');
    assert.equal(premise.antecedent.type, 'not');
  });

  it('makeCustomPremise bracketed combination', function() {
    const pc = new PremiseConstructor();
    const premise = pc.makeCustomPremise('P^(R↔(~(QvS)→T))');
    assert.equal(premise.premiseString, 'P ^ (R ↔ (~(Q v S) → T))');
    assert.equal(premise.type, 'and');
  });

  it('makeCustomPremise fail', function() {
    const pc = new PremiseConstructor();
    const premise = pc.makeCustomPremise('P→');
    assert.equal(premise,
        'Bad Premise: Input Premise is not valid.');
  });

  it('stringFromJSON nested conditional with brackets', function() {
    const pc = new PremiseConstructor();
    const premJSON = {
      type: 'conditional',
      antecedent: {
        type: 'conditional',
        antecedent: {
          type: 'atomic',
          symbol: 'P',
        },
        consequent: {
          type: 'atomic',
          symbol: 'Q',
        },
      },
      consequent: {
        type: 'atomic',
        symbol: 'R',
      },
    };
    assert.equal(pc.stringFromJSON(premJSON), '(P → Q) → R');
  });

  it('stringFromJSON nested biconditional with brackets left', function() {
    const pc = new PremiseConstructor();
    const premJSON = {
      type: 'biconditional',
      premise1: {
        type: 'conditional',
        antecedent: {
          type: 'and',
          premise1: {
            type: 'atomic',
            symbol: 'P',
          },
          premise2: {
            type: 'atomic',
            symbol: 'Q',
          },
        },
        consequent: {
          type: 'atomic',
          symbol: 'R',
        },
      },
      premise2: {
        type: 'conditional',
        antecedent: {
          type: 'atomic',
          symbol: 'R',
        },
        consequent: {
          type: 'and',
          premise1: {
            type: 'atomic',
            symbol: 'P',
          },
          premise2: {
            type: 'atomic',
            symbol: 'Q',
          },
        },
      },
    };
    assert.equal(pc.stringFromJSON(premJSON), '(P ^ Q) ↔ R');
  });

  it('stringFromJSON nested biconditional with brackets right', function() {
    const pc = new PremiseConstructor();
    const premJSON = {
      type: 'biconditional',
      premise2: {
        type: 'conditional',
        antecedent: {
          type: 'and',
          premise1: {
            type: 'atomic',
            symbol: 'P',
          },
          premise2: {
            type: 'atomic',
            symbol: 'Q',
          },
        },
        consequent: {
          type: 'atomic',
          symbol: 'R',
        },
      },
      premise1: {
        type: 'conditional',
        antecedent: {
          type: 'atomic',
          symbol: 'R',
        },
        consequent: {
          type: 'and',
          premise1: {
            type: 'atomic',
            symbol: 'P',
          },
          premise2: {
            type: 'atomic',
            symbol: 'Q',
          },
        },
      },
    };
    assert.equal(pc.stringFromJSON(premJSON), 'R ↔ (P ^ Q)');
  });

  it('stringFromJSON not with brackets', function() {
    const pc = new PremiseConstructor();
    const premJSON = {
      type: 'not',
      premise: {
        type: 'and',
        premise1: {
          type: 'atomic',
          symbol: 'P',
        },
        premise2: {
          type: 'atomic',
          symbol: 'Q',
        },
      },
    };
    assert.equal(pc.stringFromJSON(premJSON), '~(P ^ Q)');
  });

  it('stringFromJSON not without brackets', function() {
    const pc = new PremiseConstructor();
    const premJSON = {
      type: 'not',
      premise: {
        type: 'atomic',
        symbol: 'P',
      },
    };
    assert.equal(pc.stringFromJSON(premJSON), '~P');
  });

  it('getJoiningSymbolFromWord', function() {
    // testing what is not covered from above stringFromJSON tests.
    const pc = new PremiseConstructor();
    assert.equal(pc.getJoiningSymbolFromWord('conditional'), '→');
    assert.equal(pc.getJoiningSymbolFromWord('or'), 'v');
    assert.equal(pc.getJoiningSymbolFromWord('not'), '~');
  });

  it('getPremiseFromJSON conditional, or, not', function() {
    const pc = new PremiseConstructor();
    const premiseP = new Premise({type: 'atomic', premise1: 'P'});
    const premiseR = new Premise({type: 'atomic', premise1: 'R'});
    const premiseQ = new Premise({type: 'atomic', premise1: 'Q'});
    const premiseNot = new Premise({type: 'not', premise1: premiseP});
    const premiseOr = new Premise({
      type: 'or',
      premise1: premiseNot,
      premise2: premiseR,
    });
    const premiseCond = new Premise({
      type: 'conditional',
      premise1: premiseOr,
      premise2: premiseQ,
    });
    const premJSON = {
      type: 'conditional',
      antecedent: {
        type: 'or',
        premise1: {
          type: 'not',
          premise: {
            type: 'atomic',
            symbol: 'P',
          },
        },
        premise2: {
          type: 'atomic',
          symbol: 'R',
        },
      },
      consequent: {
        type: 'atomic',
        symbol: 'Q',
      },
    };
    const newPremise = pc.getPremiseFromJSON(premJSON);
    assert.equal(newPremise.equalsPremise(premiseCond), true);
  });

  it('makeCustomPremise double negated', function() {
    const pc = new PremiseConstructor();
    const premise = pc.makeCustomPremise('~~P');
    assert.equal(premise.premiseString, '~~P');
    assert.equal(premise.type, 'not');
    assert.equal(premise.premise.type, 'not');
  });
});
