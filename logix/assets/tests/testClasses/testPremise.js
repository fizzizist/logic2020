import {expect, assert} from 'chai';
import Premise from '../../logix/classes/Premise';

describe('Premise tests', function() {
  const premiseA = new Premise({type: 'atomic', premise1: 'A'});
  const premiseB = new Premise({type: 'atomic', premise1: 'B'});
  const premiseCond = new Premise({
    type: 'conditional',
    premise1: premiseA,
    premise2: premiseB,
  });
  const premiseCond2 = new Premise({
    type: 'conditional',
    premise1: premiseB,
    premise2: premiseA,
  });
  const premiseBicond = new Premise({
    type: 'biconditional',
    premise1: premiseCond,
    premise2: premiseCond2,
  });
  const premiseAnd = new Premise({
    type: 'and',
    premise1: premiseA,
    premise2: premiseB,
  });
  const premiseOr = new Premise({
    type: 'or',
    premise1: premiseA,
    premise2: premiseB,
  });
  const premiseNot = new Premise({
    type: 'not',
    premise1: premiseA,
  });

  it('getPremiseString for atomic', function() {
    assert(premiseA.premiseString === 'A');
  });

  it('getPremiseString for conditional', function() {
    assert(premiseCond.premiseString === 'A → B');
  });

  it('getPremiseString for biconditional', function() {
    assert(premiseBicond.premiseString === 'A ↔ B');
  });

  it('getPremiseString for and', function() {
    assert(premiseAnd.premiseString === 'A ^ B');
  });

  it('getPremiseString for or', function() {
    assert(premiseOr.premiseString === 'A v B');
  });

  it('getPremiseString for not', function() {
    assert(premiseNot.premiseString === '~A');
  });

  it('getType', function() {
    assert(premiseA.type === 'atomic');
  });

  it('setID, getID', function() {
    premiseA.id = 'PR1';
    assert(premiseA.id === 'PR1');
  });

  it('getAntecedent', function() {
    const newPremise = premiseCond.antecedent;
    assert(newPremise.premiseString === 'A');
  });

  it('getConsequent', function() {
    const newPremise = premiseCond.consequent;
    assert(newPremise.premiseString === 'B');
  });

  it('setCommandText, getCommandText', function() {
    premiseA.commandText = 'PR1 PR2 MP';
    assert(premiseA.commandText === 'PR1 PR2 MP');
  });

  it('equalsPremise atomic true', function() {
    assert(premiseA.equalsPremise(premiseCond.antecedent) === true);
  });

  it('equalsPremise atomic false', function() {
    assert(premiseA.equalsPremise(premiseCond.consequent) === false);
  });

  it('equalsPremise wrong type', function() {
    assert(premiseA.equalsPremise(premiseCond) === false);
  });

  it('equalsPremise conditional true', function() {
    const newPremise = new Premise({
      type: 'conditional',
      premise1: premiseA,
      premise2: premiseB,
    });
    assert(newPremise.equalsPremise(premiseCond) === true);
  });

  it('equalsPremise conditional false', function() {
    const newPremise = new Premise({
      type: 'conditional',
      premise1: premiseB,
      premise2: premiseA,
    });
    assert(newPremise.equalsPremise(premiseCond) === false);
  });

  it('equalsPremise biconditional true', function() {
    const newPremise = new Premise({
      type: 'biconditional',
      premise1: premiseCond,
      premise2: premiseCond2,
    });
    assert(newPremise.equalsPremise(premiseBicond) === true);
  });

  it('equalsPremise biconditional false', function() {
    const newPremise = new Premise({
      type: 'biconditional',
      premise1: premiseCond,
      premise2: premiseA,
    });
    assert(newPremise.equalsPremise(premiseBicond) === false);
  });

  it('equalsPremise "not" true', function() {
    const newPremise = new Premise({
      type: 'not',
      premise1: premiseA,
    });
    assert(newPremise.equalsPremise(premiseNot) === true);
  });

  it('equalsPremise "not" false', function() {
    const newPremise = new Premise({
      type: 'not',
      premise1: premiseB,
    });
    assert(newPremise.equalsPremise(premiseNot) === false);
  });

  it('toJSON atomic', function() {
    expect(premiseA.toJSON()).to.eql({
      id: 'PR1',
      type: 'atomic',
      symbol: 'A'});
  });

  it('toJSON conditional (nesting)', function() {
    expect(premiseCond.toJSON()).to.eql({
      id: null,
      type: 'conditional',
      antecedent: {
        id: 'PR1',
        type: 'atomic',
        symbol: 'A',
      },
      consequent: {
        id: null,
        type: 'atomic',
        symbol: 'B',
      },
    });
  });
});
