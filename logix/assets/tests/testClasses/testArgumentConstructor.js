const assert = require('assert');
import ArgumentConstructor from '../../logix/classes/ArgumentConstructor';

describe('ArgumentConstructor tests', function() {
  it('stringFromJSON', function() {
    const ac = new ArgumentConstructor();
    const argJSON = {
      argument: {
        premises: [{
          type: 'atomic',
          symbol: 'P',
        }],
        conclusion: {
          type: 'atomic',
          symbol: 'P',
        },
      },
    };
    assert.equal(ac.stringFromJSON(argJSON), 'P. ∴ P.');
  });
});
