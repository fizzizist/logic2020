from django.db import models
from django.contrib.postgres.fields import ArrayField


class Command(models.Model):
    """A command given for a particular row of a derivation"""

    class Meta:
        unique_together = ("line_num", "derivation")

    parent_show = models.ForeignKey(
        "logix.Show", on_delete=models.CASCADE, related_name="commands"
    )
    child_show = models.ForeignKey(
        "logix.Show", on_delete=models.CASCADE, null=True, related_name="parent_command"
    )
    terms = ArrayField(models.TextField())
    line_num = models.IntegerField()
    derivation = models.ForeignKey("logix.Derivation", on_delete=models.CASCADE)
