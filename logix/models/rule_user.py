from django.db import models
from django.contrib.auth.models import User
from logix.models import Rule


class RuleUser(models.Model):
    """Join tabloe between Rule and User django models."""
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    rule = models.ForeignKey(Rule, on_delete=models.CASCADE)
    unlocked = models.BooleanField(default=False)
