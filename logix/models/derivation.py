from django.db import models
from django.contrib.auth.models import User


class Derivation(models.Model):
    """Model representing a derivation of an argument by a user."""

    argument = models.ForeignKey("logix.Argument", on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    solved = models.BooleanField(default=False)
    show_conc = models.ForeignKey("logix.Show", on_delete=models.SET_NULL, null=True)
