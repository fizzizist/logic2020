from django.db import models


class Show(models.Model):
    """Represents a Show statement inside a Derivation. These can be nested"""

    shown = models.BooleanField(default=False)
