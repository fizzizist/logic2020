from django.db import models


class Rule(models.Model):
    """Rule Django model for holding Logic Rule information,"""
    label = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=50, unique=True)
    description = models.TextField(unique=True, null=True)
    command = models.CharField(max_length=50, unique=True)
    english = models.TextField(null=True)
    symbolization = models.CharField(max_length=50, null=True)
    unlocked_by = models.CharField(max_length=200, null=True)
