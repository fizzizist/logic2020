"""Module for Argument Django Model."""
from django.db import models
from logix.enums import ArgumentSet
from logix.models import Rule


class Argument(models.Model):
    """Argument Django Model."""
    argument = models.JSONField(unique=True)
    argument_set = models.IntegerField(choices=ArgumentSet.choices, default=ArgumentSet.SET1)
    rule = models.OneToOneField(Rule, on_delete=models.SET_NULL, null=True)
