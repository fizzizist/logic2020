const path = require('path');
const webpack = require('webpack');
const BundleTracker = require('webpack-bundle-tracker');

module.exports = {
  context: __dirname,

  target: 'web',

  entry: {
    deriver: './logix/assets/logix/index.jsx',
    argumentConstructor: './logix/assets/argumentConstructor/index.jsx',
  },

  output: {
    path: path.resolve('./logix/static/logix/bundles/'),
  },

  plugins: [
    new BundleTracker({filename: 'webpack-stats.json'}),
  ],
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
    ],
  },
  resolve: {
    modules: ['node_modules'],
    extensions: ['*', '.js', '.jsx'],
  },
};
